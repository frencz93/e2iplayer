# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import urllib_quote
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'Brstej'


class Brstej(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'brstej', 'cookie': 'brstej.cookie'})

        self.MAIN_URL = 'https://ser.brstej.com/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/R2cDy5r/brstej.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("Brstej.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category102.php?cat=movies2024')},
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'other', 'title': _('رمــــضــان'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'tvshow', 'title': _('بــرامــج'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category102.php?cat=tv2024')},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG("Brstej.listCatItems cItem[%s]" % (cItem))
        category = self.currItem.get("category", '')

        if category == 'serie':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('مسلسلات عربية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category102.php?cat=aarab-2024')},
                {'category': 'list_items', 'title': _('مسلسلات تركية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category102.php?cat=tyyy1-2024')},
                {'category': 'list_items', 'title': _('مسلسلات مصرية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category102.php?cat=eeg12024')},
                {'category': 'list_items', 'title': _('مسلسلات شامية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category102.php?cat=syy1-2024')},
                {'category': 'list_items', 'title': _('مسلسلات خليجية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category102.php?cat=5a1-2024')},
                {'category': 'list_items', 'title': _('مسلسلات إنمي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category102.php?cat=anmei')},]
        elif category == 'other':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('رمــــضــان 2024'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category102.php?cat=rammdan1-2024')},
                {'category': 'list_items', 'title': _('رمــــضــان 2023'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category102.php?cat=rammmda1-1n2023')},]
        self.listsTab(NEW_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG("Brstej.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'pagination'), ('</ul', '>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(self.ph.decodeHtml(nextPage), '''href=['"]([^'^"]+?)['"][^>]*?>%s<''' % (page + 1))[0])

        titlesTab = []
        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'pm-grid'), ('<div', '>', 'clearfix'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'thumbnail'), ('</li', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''data-echo=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''<a href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('ellipsis', '>'), ('</h3', '>'), False)[1])

            info = self.ph.std_title(title.split('الحلقة')[0])
            if title != '':
                title = info.get('title_display')
            desc = info.get('desc')

            if title not in titlesTab:
                titlesTab.append(title)
                params = dict(cItem)
                params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
                self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("Brstej.exploreItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        Season = self.cm.ph.getDataBeetwenNodes(data, ('<div', '>', 'SeasonsEpisodesMain'), ('<div', '>', 'clearfix'), True)[1]
        tmpSeason = self.cm.ph.getAllItemsBeetwenMarkers(Season, ('<div', '>', 'data-serie'), ('</div', '>'))
        if tmpSeason:
            for itemS in tmpSeason:
                title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(itemS, '''data-serie=['"]([^"^']+?)['"]''')[0])
                self.addMarker({'title': '%s Season' % E2ColoR('lime'), 'icon': cItem['icon'], 'desc': ''})

                tmp = self.cm.ph.getAllItemsBeetwenMarkers(itemS, '<a', ('</a', '>'))
                for item in tmp:
                    url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                    title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])

                    info = self.ph.std_title(title, with_ep=True)
                    if title != '':
                        title = info.get('title_display')
                    desc = info.get('desc')

                    params = dict(cItem)
                    params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': desc})
                    self.addVideo(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': cItem['desc']})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("Brstej.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        url = self.getFullUrl('/search.php?keywords={}'.format(urllib_quote(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG("Brstej.getLinksForVideo [%s]" % cItem)
        urlTab = []

        baseUrl = cItem['url'].replace("watch.php", "play.php")
        sts, data = self.getPage(baseUrl)
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'WatchServers'), ('</div', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, '<button', ('</button', '>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''data-embed-url=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('</span', '>'), ('</button', '>'), False)[1])

            if title != '':
                title = ('{} {} [{}]{}{} - {}{}'.format(cItem['title'], E2ColoR('lightred'), title, E2ColoR('white'), E2ColoR('yellow'), self.up.getHostName(url, True), E2ColoR('white')))

            urlTab.append({'name': title, 'url': url, 'need_resolve': 1})

        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("Brstej.getVideoLinks [%s]" % videoUrl)

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

    # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'serie' or category == 'other':
            self.listCatItems(self.currItem)
        elif category == 'list_items' or category == 'tvshow' or category == 'movei':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, Brstej(), True, [])
