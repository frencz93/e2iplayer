# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Components.config import ConfigText, config
from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'Alarabiya'


class Alarabiya(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'cookie': 'alarabiyanet.cookie'})

        self.MAIN_URL = 'https://www.alarabiya.net/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/smkDkR8/alarabiya.png'

        config.plugins.iptvplayer.cloudflare_user = ConfigText(default='Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0', fixed_size=False)
        self.USER_AGENT = config.plugins.iptvplayer.cloudflare_user.value
        self.HTTP_HEADER = {'User-Agent': self.USER_AGENT, 'DNT': '1', 'Accept': 'text/html', 'Accept-Encoding': 'gzip, deflate', 'Referer': self.getMainUrl(), 'Origin': self.getMainUrl(), 'Upgrade-Insecure-Requests': '1', 'Connection': 'keep-alive'}
        self.AJAX_HEADER = dict(self.HTTP_HEADER)
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest', 'Accept-Encoding': 'gzip, deflate', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8', 'Accept': 'application/json, text/javascript, */*; q=0.01'})
        self.defaultParams = {'header': self.HTTP_HEADER, 'with_metadata': True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPageCFProtection(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        if data.meta.get('cf_user', self.USER_AGENT) != self.USER_AGENT:
            self.__init__()
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("Alarabiya.listMainMenu")
        MAIN_CAT_TAB = [{'category': 'show_movies', 'title': _('افـلام وثائقية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/programs/documentaries/')}]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG("Alarabiya.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'pager'), ('<i', '>', 'aa-chevron-right'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''<a href=['"]([^"^']+?)['"] rel=['"]next['"]''')[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'video-list'), ('</ul', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', '>', 'list-item'), ('</li', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0]).replace("|", "").replace("الفيلم الوثائقي", "").replace("وثائقي", "")
            desc = self.ph.extract_desc(item, [('year', '''caption['"].+?([0-9]{4})[$<]''')])

            info = self.ph.std_title(title, desc=desc, with_ep=True)
            if title != '':
                title = info.get('title_display')
            desc = info.get('desc')

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("Alarabiya.exploreItems cItem[%s]" % (cItem))

        params = dict(cItem)
        params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': cItem['desc']})
        self.addVideo(params)

    def getLinksForVideo(self, cItem):
        printDBG("Alarabiya.getLinksForVideo [%s]" % cItem)
        urlTab = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        url = self.getFullUrl(self.cm.ph.getSearchGroups(data, '''['"]contentUrl['"]: ['"]([^"^']+?)['"]''')[0])
        urlTab.append({'name': '', 'url': url, 'need_resolve': 0})
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("Alarabiya.getVideoLinks [%s]" % videoUrl)

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("Alarabiya.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('card-content-hover', '>'), ('</span', '</a'), True)[1]

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('card-description', '>'), ('</p', '</div>'), False)[1])
        if desc == '':
            desc = cItem['desc']

        return [{'title': cItem['title'], 'text': desc, 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

        # MAIN MENU
        if name is None and category == '':
            self.cm.clearCookie(self.COOKIE_FILE, ['__cf_bm', 'beraredirectar', 'cf_clearance'])
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'show_movies':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, Alarabiya(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
