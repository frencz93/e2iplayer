# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'Akoam'


class Akoam(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'akoam', 'cookie': 'akoam.cookie'})

        self.MAIN_URL = 'https://ak.sv/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/vB9nWss/akoam.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams is {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("Akoam.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'other', 'title': _('رمـــضــان'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'tvshow', 'title': _('بــرامــج'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG(f"Akoam.listCatItems cItem[{cItem}]")
        category = self.currItem.get("category", '')

        if category is 'movei':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('أفلام أجنبية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/156/الأفلام-الاجنبية')},
                {'category': 'list_items', 'title': _('أفلام عربية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/155/الأفلام-العربية')},
                {'category': 'list_items', 'title': _('أفلام أسيوية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/171/الافلام-الاسيوية')},
                {'category': 'list_items', 'title': _('أفلام هندية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/168/الافلام-الهندية')},
                {'category': 'list_items', 'title': _('سلاسل الافلام'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/186/سلاسل-الافلام-الاجنبية')},
                {'category': 'list_items', 'title': _('أفلام وثائقية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/94/افلام-وثائقية')},
                {'category': 'list_items', 'title': _('أفلام انمي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/179/افلام-الانمي-المدبلجة')},]
        elif category is 'serie':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('مسلسلات أجنبية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/166/المسلسلات-الاجنبية')},
                {'category': 'list_items', 'title': _('مسلسلات عربية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/80/المسلسلات-العربية')},
                {'category': 'list_items', 'title': _('مسلسلات أسيوية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/185/المسلسلات-الاسيوية')},
                {'category': 'list_items', 'title': _('مسلسلات مدبلجة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/190/المسلسلات-المدبلجة')},
                {'category': 'list_items', 'title': _('مسلسلات انمي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/178/مسلسلات-الانمي')},]
        elif category is 'other':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('رمضان 2019'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/194/مسلسلات-و-برامج-رمضان-2019')},
                {'category': 'list_items', 'title': _('رمضان 2018'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/187/مسلسلات-و-برامج-رمضان-2018')},
                {'category': 'list_items', 'title': _('رمضان 2017'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/184/رمضان-2017')},
                {'category': 'list_items', 'title': _('رمضان 2016'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/182/رمضان-2016')},
                {'category': 'list_items', 'title': _('رمضان 2015'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/176/رمضان-2015')},
                {'category': 'list_items', 'title': _('رمضان 2014'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/169/رمضان-2014')},]
        elif category is 'tvshow':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('برامج تلفزيونية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/81/البرامج-التلفزيونية')},
                {'category': 'list_items', 'title': _('مصارعة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/88/المصارعة-الحرة')},
                {'category': 'list_items', 'title': _('مسرحيات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/old/cat/149/مسرحيات')}]
        self.listsTab(NEW_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG(f"Akoam.listItems cItem[{cItem}]")
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'pagination'), ('</ul', '>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^'^"]+?)['"][^>]*?>{}<'''.format(page + 1))[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'navigation'), ('<div', '>', 'subject_box no_tile'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'subject_box'), ('</div', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<h3', '>'), ('</h3', '>'), False)[1])
            desc = self.ph.extract_desc(item, [('plot', '''desc['"].+?([^>]+?)[$<]''')])

            if 'توضيح هام' in title:
                continue

            info = self.ph.std_title(title, desc=desc, with_ep=True)
            if title is not '':
                title = info.get('title_display')
            desc = info.get('desc')

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addDir(params)

        if nextPage is not '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def listFilters(self, cItem):
        printDBG(f"Akoam.listFilters cItem{cItem}]")
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'pagination'), ('</ul', '>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^'^"]+?)['"][^>]*?>{}<'''.format(page + 1))[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'akoam_result'), ('<footer', '>', 'footer'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'tags_box'), ('</a', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''url\((.+?)\)''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<h1', '>'), ('</h1', '>'), False)[1])

            if 'توضيح هام' in title:
                continue

            info = self.ph.std_title(title, with_ep=True)
            if title is not '':
                title = info.get('title_display')
            desc = info.get('desc')

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addDir(params)

        if nextPage is not '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG(f"Akoam.exploreItems cItem[{cItem}]")

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        # trailer
        trailerUrl = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'youtube-player'), ('</div', '>'))[1]
        trailerUrl = self.cm.ph.getSearchGroups(trailerUrl, '''data-id=['"]([^"^']+?)['"]''')[0]
        trailerUrl = f'https://www.youtube.com/embed/{trailerUrl}'
        if self.cm.isValidUrl(trailerUrl):
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': f'[{E2ColoR('yellow')}Trailer{E2ColoR('white')}]', 'url': trailerUrl, 'desc': ''})
            self.addVideo(params)

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('sub_desc', '</span>'), ('<br', '/>'), False)[1])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'ad-300-250'), ('<a', '>', 'sub_alert'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'direct_link_box'), ('</a', '>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('sub_file_title', '>'), ('<i', '>'), False)[1])
            if title is '':
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('sub-no-file', '>'), ('<br', '>'), False)[1])

            title = title.split('akoam', 1)[0].split('akwam', 1)[0].replace('.', ' ')
            info = self.ph.std_title(title, with_ep=True)
            if title is not '':
                title = info.get('title_display')
            otherInfo = '{}\n{}'.format(info.get('desc'), desc)

            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG(f"Akoam.listSearchResult cItem[{cItem}], searchPattern[{searchPattern}] searchType[{searchType}]")
        if searchType is 'all':
            url = self.getFullUrl(f'/old/search/{urllib_quote_plus(searchPattern)}')
        elif searchType is 'movies':
            url = self.getFullUrl(f'/old/search/فيلم+{urllib_quote_plus(searchPattern)}')
        elif searchType is 'series':
            url = self.getFullUrl(f'/old/search/مسلسل+{urllib_quote_plus(searchPattern)}')
        params = {'name': 'category', 'category': 'list_search', 'type': searchType, 'good_for_fav': False, 'url': url}
        self.listFilters(params)

    def getLinksForVideo(self, cItem):
        printDBG(f"Akoam.getLinksForVideo [{cItem}]")
        urlTab = []

        if 'Trailer' in cItem['title']:
            return self.up.getVideoLinkExt(cItem['url'])

        baseUrl = cItem['url'].replace('download', 'watching')

        params = dict(self.defaultParams)
        params['header'] = dict(self.AJAX_HEADER)
        params['header']['Referer'] = baseUrl
        params['header']['Origin'] = 'ak.sv'
        params['header']['Host'] = 'ak.sv'

        sts, data = self.getPage(baseUrl, params)
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'player'), ('</script', '>'), True)[1]
        url = self.getFullUrl(self.cm.ph.getSearchGroups(tmp, '''file.+?['"]([^"^']+?)['"]''')[0])

        urlTab.append({'name': '', 'url': url, 'need_resolve': 0})

        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG(f"Akoam.getVideoLinks [{videoUrl}]")

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG(f"Akoam.getArticleContent [{cItem}]")
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'sub_mainInfo'), ('</div', '>'), True)[1]

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('imdb', '>'), ('</span', '>'), False)[1])
        if Info is not '':
            otherInfo['imdb_rating'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('اللغة', ':'), ('</span', '>'), False)[1])
        if Info is not '':
            otherInfo['language'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('الترجمة', ':'), ('</span', '>'), False)[1])
        if Info is not '':
            otherInfo['translation'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('جودة', ':'), ('</span', '>'), False)[1])
        if Info is not '':
            otherInfo['quality'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('محتوى', ':'), ('</span', '>'), False)[1])
        if Info is not '':
            otherInfo['content'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('سنة الانتاج', ':'), ('</span', '>'), False)[1])
        if Info is not '':
            otherInfo['year'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('المدة', ':'), ('</span', '>'), False)[1])
        if Info is not '':
            otherInfo['duration'] = Info

        return [{'title': cItem['title'], 'text': cItem['desc'], 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG(f"handleService: |||||||||||||||||||||||||||||||||||| name[{name}], category[{category}] ")
        self.currList = []

    # MAIN MENU
        if name is None and category is '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category is 'movei' or category is 'serie' or category is 'tvshow' or category is 'other':
            self.listCatItems(self.currItem)
        elif category is 'list_items':
            self.listItems(self.currItem)
        elif category is 'list_search':
            self.listFilters(self.currItem)
        elif category is 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category is "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, Akoam(), True, [])

    def getSearchTypes(self):
        searchTypesOptions = []
        searchTypesOptions.append(("All", "all"))
        searchTypesOptions.append(("Movies", "movies"))
        searchTypesOptions.append(("Tv Series", "series"))
        return searchTypesOptions

    def withArticleContent(self, cItem):
        if cItem['type'] is not 'video' and cItem['category'] is not 'exploreItems':
            return False
        return True
