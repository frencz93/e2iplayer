# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'Cinemana'


class Cinemana(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'cinemana', 'cookie': 'cinemana.cookie'})

        self.MAIN_URL = 'https://cinemana.work/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/fH4LnHX/cinemana.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("Cinemana.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'wwe', 'title': _('مــصارعــة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/watch=category/مصارعة-حرة/')},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG("Cinemana.listCatItems cItem[%s]" % (cItem))
        category = self.currItem.get("category", '')

        if category == 'movei':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('أفــلام أجنـبيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies/')},
                {'category': 'list_items', 'title': _('أفــلام عــربــيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/page/arabic/')},]
        elif category == 'serie':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('رمــضـــان'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/watch=category/مسلسلات-رمضان-2024/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات أجنـبيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/watch=category/مسلسلات-اجنبي/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات عــربــيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/watch=category/مسلسلات-عربية/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات تـركـيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/watch=category/مسلسلات-تركية/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات آسـيـويـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/watch=category/مسلسلات-اسيوية/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات هنــديـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/watch=category/مسلسلات-هندية/')}]
        self.listsTab(NEW_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG("Cinemana.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'Paginate'), ('</div', '>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^'^"]+?)['"][^>]*?>%s<''' % (page + 1))[0])

        titlesTab = []
        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'ArcPage'), ('<div', '>', 'clear'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'ItemBlock'), ('</a', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''url\((.+?)\)''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<h3', '>'), ('</h3', '>'), False)[1])

            info = self.ph.std_title(title)
            if title != '':
                title = info.get('title_display')
            desc = info.get('desc')

            if title not in titlesTab:
                titlesTab.append(title)
                params = dict(cItem)
                params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
                self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("Cinemana.exploreItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<div', '>', 'Story'), ('</div', '>'), False)[1])

        Season = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'tab-content season-scroll'), ('<script', '>', 'text/javascript'), True)[1]
        if Season:
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(Season, ('<li', '>'), ('</li', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</a', '>'), False)[1])

                info = self.ph.std_title(title, with_ep=True)
                otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                self.addVideo(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("Cinemana.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        url = self.getFullUrl('/?s={}&type=all'.format(urllib_quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG("Cinemana.getLinksForVideo [%s]" % cItem)
        urlTab = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmpID = self.cleanHtmlStr(self.cm.ph.getSearchGroups(data, '''data-id=['"]([^"^']+?)['"]''')[0])
        tmpUrl = self.getFullUrl('/wp-content/themes/EEE/Inc/Ajax/Single/Server.php')

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'ServersList'), ('</div', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, '<li', ('</li', '>'))
        for item in tmp:
            tmpServ = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-server=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('data-server', '>'), ('</li', '>'), False)[1])

            sts, data = self.getPage(tmpUrl, post_data={'post_id': tmpID, 'server': tmpServ})
            if not sts:
                return

            tmp = data.replace('\n', '').replace('\r', '')
            url = self.getFullUrl(self.cm.ph.getSearchGroups(tmp, '''SRC=['"]([^"^']+?)['"]''', ignoreCase=True)[0].replace(" ", ""))

            if title != '':
                title = ('{} {} [{}]{}{} - {}{}'.format(cItem['title'], E2ColoR('lightred'), title, E2ColoR('white'), E2ColoR('yellow'), self.up.getHostName(url, True), E2ColoR('white')))

            urlTab.append({'name': title, 'url': strwithmeta(url, {'Referer': self.getMainUrl()}), 'need_resolve': 1})

        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("Cinemana.getVideoLinks [%s]" % videoUrl)

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("Cinemana.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<aside', '>', 'SingleAside'), ('<script', '>', 'text/javascript'), True)[1]

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('<i', '>', 'fa fa-star'), ('</span', '>'), False)[1]).replace('/', '')
        if Info != '':
            otherInfo['imdb_rating'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('genre', '>'), ('</li', '>'), False)[1])
        if Info != '':
            otherInfo['genre'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('<i', '>', 'fa-clock'), ('</span', '>'), False)[1])
        if Info != '':
            otherInfo['duration'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('quality', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['quality'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('release-year', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['year'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('category', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['category'] = Info

        return [{'title': cItem['title'], 'text': cItem['desc'], 'images': [{'title': '', 'url': self.getFullUrl(cItem['icon'])}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

    # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'movei' or category == 'serie':
            self.listCatItems(self.currItem)
        elif category == 'list_items' or category == 'wwe':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, Cinemana(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
