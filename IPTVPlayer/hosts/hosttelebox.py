# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from re import sub

from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs.e2ijson import loads as json_loads
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'Telebox'


class Telebox(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'telebox', 'cookie': 'telebox.cookie'})

        self.MAIN_URL = 'https://www.linkbox.to/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/dLqLt8f/linkbox.png'

        self.HEADER = self.cm.getDefaultHeader('firefox')
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.nb_item = 50
        self.item_count = 0

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("Telebox.listMainMenu")

        sList = [
            ('2024 اشهر واقوى المسلسلات والافلام', 'nLw3cuO', '683626'),
            ('مكتبة المسلسلات والأفلام Movies and Series Library with 4k', '_ig_gOEnWNM_2543983_3648', '2543983'),
            ('Anime Box', 'app01e2f1adf1aca0a1a1a4a7a2a0adf2aca0a1a1a4a7a2a0', '6772146'),
            ('Htm.Animes', 'app01e2f1adf2acaeafa1a3a1a0a0adf1acaeafa1a3a1a0a0', '8975766'),
            ('Cinema Baghdad', 'UiLE7sU', '614406'),
            ('Cinema-Club (أفلام)', 'app01e2f1adf1aca4a3a2a1a7aea4adf2aca4a3a2a1a7aea4', '2547182'),
            ('Cinema-Club (مسلسلات)', 'app01e2f1adf1aca4a1afaea4a5a1adf2aca4a1afaea4a5a1', '2798237'),
            ('Cinema-Club (أفلام و مسلسلات الأنمي)', 'app01e2f1adf1aca4a3a5aea3aea0adf2aca4a3a5aea3aea0', '2538586'),
            ('Cinema Crown', 'app01e2f1adf1aca7a4a5a3a1a3aeadf2aca7a4a5a3a1a3ae', '1235758'),
            ('cimaabdo', '_ig_esEuECt_609855_299a', '609855'),
            ('Egybest', 'xgLMOew', '7040626'),
            ('EGY-BEST', 'app01e2f1adf2aca7a3a6a0a1a1a5adf1aca7a3a6a0a1a1a5', '1506773'),
            ('ايجي بست EgyBest', '_ig_app01e2f1adf0f2acf0e6a5fbf9e5a6a6a6eefdf8_3589656_8ed8', '3589656'),
            ('MARVEL MOROCCO', 'SD9p5bO', '6496365'),
            ('Movies Plus - أفلام', 'app01e2f1adf1aca0a2a3a1a0a6adf2aca0a2a3a1a0a6', '645760'),
            ('Netflix', '_ig_app01e2f1adf0f2acf0e6a5fda0aea6a6a6f3afe0_2674587_0ddd', '2674587'),
            ('افلام و مسلسلات netflix', '_ig_2z1IFpK_4702801_f98c', '4702801'),
            ('Netfilx (2022) أفلام ومسلسلات متنوعة', 'app01e2f1adf2acaea1aea1afa0adf1acaea1aea1afa0', '878796'),
            ('New Q', '_ig_app01e2f1adf0f2acf0e6a5fafae5a6a6f4f2aff4_4606358_e7ba', '4606358'),
            ('ONE Cima TV', 'app01e2f1adf1aca7a5a2a5a5a4a2adf2aca7a5a2a5a5a4a2', '1343324'),
            ('Star Cinema', 'app01e2f1adf1aca1afaea6a7adf2aca1afaea6a7', '79801'),
            ('Showtime Movies', 'app01e2f1adf1aca2a4a7afa5afa7adf2aca2a4a7afa5afa7', '4219391'),
            ('The Movie Night', 'app01e2f1adf2acaeafa3a3afa3adf1acaeafa3a3afa3', '895595'),
            ('English Films - افلام اجنبي', '_ig_app01e2f1adf0f2acf0e6a5f1e2a6a6a6a5a6efe3_752951_b7af', '752951'),
            ('THROW LOB(رياضة)', 'NZKr9gl', '607863'),
            ('The Movie Muse', 'ZmM9DaP', '6896445'),
            ('Yalla Anime', 'app01e2f1adf2aca4afa3a5a1a5aeadf1aca4afa3a5a1a5ae', '2953738'),
            ('World1Movies', 'app01e2f1adf2aca0a6a7a5aea3adf1aca0a6a7a5aea3', '601385'),
            ('مجتمع الأفلام والمسلسلات | Documentary Films', 'app01e2f1adf2acaea7a5a2a3a5a4adf1acaea7a5a2a3a5a4', '8134532'),
            ('فرجني شكرا - Faragny', '_ig_app01e2f1adf0f2acf0e6a5fcaff5a6a6a6afa7fe_2609502_fdae', '2609502'),
            ('Marvel Movies أفلام و مسلسلات', '_ig_app01e2f1adf0f2acf0e6a5fbf1fda6a6a2f7a6fa_935938_2c28', '935938'),
            ('افلام shof_ha', 'app01e2f1adf2aca4afa5a5a6a2aeadf1aca4afa5a5a6a2ae', '2933048'),
            ('يلا Movies', 'app01e2f1adf2aca3a5afa1aea4a4adf1aca3a5afa1aea4a4', '5397822'),
            ('سيما هاوس & cima house', 'app01e2f1adf1aca7a2a0a2a3a0a2adf2aca7a2a0a2a3a0a2', '1464564'),
            ('موطن المفيز movies المدبلج', 'app01e2f1adf2aca0aea6a2a5a3adf1aca0aea6a2a5a3', '680435'),
            ('سينما موفيز', 'app01e2f1adf1aca7a6a2a6a0a0a3adf2aca7a6a2a6a0a0a3', '1040665'),
            ('سينما أونلاين', 'app01e2f1adf1aca5a2aea1a2a6a3adf2aca5a2aea1a2a6a3', '3487405'),
            ('عشاق الافلام', 'app01e2f1adf2acafafa4a2a3a2adf1acafafa4a2a3a2', '992454'),
            ('إجي بيست', 'DERRaVk', '3767665'),
            ('اجي بيست', '_ig_app01e2f1adf0f2acf0e6a5f9faa2a6a6a2e6f4e2_2751140_2b6c', '2751140'),
            ('مسلسلات:دراما نيوز', '_ig_app01e2f1adf0f2acf0e6a5fde3aea6a6a6aff7f2_3576258_c91a', '3576258'),
            ('⏎مــــــسلســــــلات ⇍كل المسلــسلات والافـلام', 'app01e2f1adf1aca3a7afa4a0a0a0adf2aca3a7afa4a0a0a0', '5192666'),
            ('أفلام ومسلسلات أجنبية', '_ig_app01e2f1adf0f2acf0e6a5fae2fda6a6a6f1a3ae_1077534_cc7b', '1077534'),
            ('مسلسلات أجنبية أكشن إثارة', '_ig_app01e2f1adf0f2acf0e6a5fbe6fda6a6a6eef4ff_6032611_496c', '6032611'),
            ('تلفازك المتنقل', '_ig_app01e2f1adf0f2acf0e6a5fdf3aea6a6a5e6f8af_3519730_d7ac', '3519730'),
            ('جميع الاقسام دراماتك', '_ig_app01e2f1adf0f2acf0e6a5faf1a6a6a6a5a3ecf8_4462318_8a7c', '4462318'),
            ('الربيعي موفيز', 'app01e2f1adf2aca3a5a5a4a1a0a1adf1aca3a5a5a4a1a0a1', '5332767'),
            ('أفلام مجان نت', 'app01e2f1adf2aca4a7a5a7a0aeafadf1aca4a7a5a7a0aeaf', '2131689'),
            ('مسلسلات و أفلام2023', 'app01e2f1adf2aca5aea1a5aea0a2adf1aca5aea1a5aea0a2', '3873864'),
            ('كيدراما (الأسيوية)', 'app01e2f1adf1aca4a3a6afa0a6adf2aca4a3a6afa0a6', '250960'),
            ('مسلسلات وأفلام أسيوية', 'app01e2f1adf2aca0a0a0afa0aeadf1aca0a0a0afa0ae', '666968'),
            ('إستراحة المنوعات', 'app01e2f1adf2aca5a4afa7a7aea6adf1aca5a4afa7a7aea6', '3291180'),
            ('عرب سينما', 'app01e2f1adf2aca5a5a3a4aea0adf1aca5a5a3a4aea0', '335286'),
            ('سلاسل أفلام', '_ig_app01e2f1adf0f2acf0e6a5fbefe1a6a6fca0fff2_728149_b0d7', '728149'),
            ('تسس موفيز', 'app01e2f1adf2aca5a2a6afafa7adf1aca5a2a6afafa7', '340991'),
            ('موفيز لاند', 'app01e2f1adf1aca1afafa4a7a7adf2aca1afafa4a7a7', '799211'),
            ('اكوام افلام ومسلسلات', 'app01e2f1adf2acafa3a4a6a3adf1acafa3a4a6a3', '95205'),
            ('جميع المسلسلات والافلام', 'app01e2f1adf2aca5a0a3a3a2a6aeadf1aca5a0a3a3a2a6ae', '3655408'),
            ('أنميات', 'app01e2f1adf2acaeaea5a2aea4afadf1acaeaea5a2aea4af', '8834829'),
            ('شانكس ساما ( إنمي)', 'app01e2f1adf2aca0aeaeafa2a1adf1aca0aeaeafa2a1', '688947'),
            ('تارو ساما ( إنمي)', 'app01e2f1adf2aca0a5a5a3a0adf1aca0a5a5a3a0', '63356'),
            ('؏ـ(🌏)ـالم اݪانــٓـــٓـمــــٴ͜ـــي', 'app01e2f1adf2aca4aea5a4a6a1a3adf1aca4aea5a4a6a1a3', '2832075'),
            ('لوفي ساما ( (إنمي', 'lUprnhl', '1287040')]
        for (title, shareToken, image) in sList:
            icon = self.getFullIconUrl('https://avatar.fuplink.net/avatar/dircover/%s' % image)

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': '', 'shareToken': shareToken, 'icon': self.cm.ph.stdUrl(icon), 'desc': ''})
            self.addDir(params)
        MAIN_CAT_TAB = [
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def exploreItems(self, cItem):
        printDBG("Telebox.exploreItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)
        pid = cItem.get('pid', 0)
        category = self.currItem.get("category", '')

        sUrl = cItem['url']
        if category != 'search':
            sUrl = self.getFullUrl('/api/file/share_out_list/?sortField=name&sortAsc=1&pageNo={}&pageSize={}&shareToken={}&pid={}&needTpInfo=1&scene=singleGroup&name=&platform=web&pf=web&lan=en'.format(page, self.nb_item, cItem['shareToken'], pid))

        sts, data = self.getPage(sUrl)
        if not sts:
            return False
        data = json_loads(data)
        if data.get('data', {}) == None:
            sts, data = self.getPage(sUrl.replace('&scene=singleGroup', '&scene=singleItem'))
            if not sts:
                return False
            data = json_loads(data)

        data = data.get('data', {})
        data = data.get('list', [])
        if not data:
            data = []

        if 'api/search' in cItem['url']:
            for item in data:
                self.item_count += 1
                title = self.cleanHtmlStr(item.get('name', ''))
                title = sub(r"[^\w\s]", "", title).replace('mp4', '')
                icon = item.get('cover', '')
                info = self.ph.std_title(title, with_ep=True)
                desc = info.get('desc')

                tmpUrl = item.get('url', '').replace('<nil>', '')
                if '/f/' in tmpUrl:
                    tmpUrl = tmpUrl.replace('/f/', '/s/')
                if ('/s/' in tmpUrl):
                    shareToken = tmpUrl.split('/s/')[1]
                    if '?pid=' in shareToken:
                        shareToken, pid = shareToken.split('?pid=')
                    else:
                        pid = ''

                    params = dict(cItem)
                    params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': '', 'shareToken': shareToken, 'pid': pid, 'icon': '', 'desc': desc})
                    self.addDir(params)
        else:
            for item in data:
                self.item_count += 1
                title = self.cleanHtmlStr(item.get('name', ''))
                title = sub(r"[^\w\s]", "", title).replace('mp4', '')
                tmpUrl = item.get('url', '').replace('<nil>', '')
                type_ = item.get('type', '')
                pid = item.get('id', '')

                icon = item.get('cover', '')
                if '&x-image-process' in icon:
                    icon = icon.split('&x-image-process', 1)[0]

                info = self.ph.std_title(title, with_ep=True)
                desc = info.get('desc')
                if (('للكبار' not in title) and ('+18' not in title)):
                    if type_ == 'dir':
                        params = dict(cItem)
                        params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': sUrl, 'shareToken': cItem['shareToken'], 'pid': pid, 'icon': cItem['icon'], 'desc': desc})
                        self.addDir(params)
                    elif type_ == 'video':
                        params = dict(cItem)
                        params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': tmpUrl, 'shareToken': cItem['shareToken'], 'pid': pid, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
                        self.addVideo(params)
                    elif type_ == 'audio':
                        params = dict(cItem)
                        params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': tmpUrl, 'shareToken': cItem['shareToken'], 'pid': pid, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
                        self.addAudio(params)
                    else:
                        printExc('No Dir Found: %s' % title)

        # if self.item_count + 1 > self.nb_item:
        #     params = dict(cItem)
        #     params.update({'title': _("Next page"), 'url': sUrl, 'shareToken': cItem['shareToken'], 'pid': pid, 'page': page + 1})
        #     self.addDir(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("Telebox.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        page = cItem.get('page', 1)

        url = 'https://www.zain3.com/api/search?kw={}&pageSize={}&pageNo={}'.format(urllib_quote_plus(searchPattern), self.nb_item, page)
        params = {'name': 'category', 'category': 'list_search', 'type': searchType, 'good_for_fav': False, 'url': url}
        self.exploreItems(params)

    def getLinksForVideo(self, cItem):
        printDBG("Telebox.getLinksForVideo [%s]" % cItem)
        urlTab = []

        urlTab.append({'name': '', 'url': cItem['url'], 'need_resolve': 0})

        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("Telebox.getVideoLinks [%s]" % videoUrl)

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

    # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'listItems' or category == 'list_search':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, Telebox(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
