# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'EgyDead'


class EgyDead(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'egydead', 'cookie': 'egydead.cookie'})

        self.MAIN_URL = 'https://egydead.space/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/kc7KTfQ/egydead.png'

        self.HEADER = self.cm.getDefaultHeader('firefox')
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.cacheLinks = {}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("EgyDead.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'other', 'title': _('الـمـدبــلــج'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'anime', 'title': _('أنــمـــي'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'tvshow', 'title': _('بـــرامج'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG("EgyDead.listCatItems cItem[%s]" % (cItem))
        category = self.currItem.get("category", '')

        sts, data = self.getPage(self.getMainUrl())
        if not sts:
            return

        if category == 'movei':
            sStart = '>الافلام'
        elif category == 'serie':
            sStart = '>مسلسلات'
        elif category == 'anime':
            sStart = '>انمي'
        elif category == 'tvshow':
            sStart = '>منوعات'
        elif category == 'other':
            sStart = '>المدبلج'

        tmp = self.cm.ph.getDataBeetwenMarkers(data, sStart, ('</ul>', '</li>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', 'menu-item'), ('</li', '>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</a', '>'), False)[1])

            params = dict(cItem)
            params.update({'category': 'list_items', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': ''})
            self.addDir(params)

        if category == 'serie':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('المسلسلات الكاملة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/serie/')},
                {'category': 'list_items', 'title': _('المواسم الكاملة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/season/')},
                {'category': 'list_items', 'title': _('Netflix'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/channel/netflix/')},]
            self.listsTab(NEW_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG("EgyDead.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'pagination'), ('</div', '>',), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^'^"]+?)['"][^>]*?>%s<''' % (page + 1))[0])

        titlesTab = []
        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'catHolder'), ('<footer', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', '>', 'movieItem'), ('</a', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])
            desc = self.ph.extract_desc(item, [('quality', '''label['"]>([^>]+?)[$<]''')])

            info = self.ph.std_title(title, desc=desc)
            if title != '':
                title = info.get('title_display')
            desc = info.get('desc')

            if title not in titlesTab:
                titlesTab.append(title)
                params = dict(cItem)
                params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
                self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("EgyDead.exploreItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        # trailer
        trailerUrl = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'trailerPopup'), ('</iframe', '>'))[1]
        trailerUrl = self.cm.ph.getSearchGroups(trailerUrl, '''src=['"]([^"^']+?)['"]''')[0]
        if self.cm.isValidUrl(trailerUrl):
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': '[%sTrailer%s]' % (E2ColoR('yellow'), E2ColoR('white')), 'url': trailerUrl, 'desc': ''})
            self.addVideo(params)

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<div', '>', 'extra-content'), ('</div', '>'), False)[1]).replace('القصه', '')

        if '/serie/' in cItem['url']:
            tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'seasons-list'), ('<div', '>', 'clr'), True)[1]
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', '>', 'movieItem'), ('</a', '>'))
            for item in tmp:
                icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])

                info = self.ph.std_title(title, with_ep=True)
                if title != '':
                    title = info.get('title_display')
                otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': otherInfo})
                self.addDir(params)

        elif '/season/' in cItem['url']:
            tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'episodes-list'), ('<div', '>', 'clr'), True)[1]
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', '>'), ('</li', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])

                info = self.ph.std_title(title, with_ep=True)
                if title != '':
                    title = info.get('title_display')
                otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                self.addVideo(params)
        else:
            if 'assembly' in cItem['url']:
                tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'singleBottomArea'), ('<div', '>', 'clr'), True)[1]
                tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', '>', 'movieItem'), ('</a', '>'))
                for item in tmp:
                    icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
                    url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                    title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])

                    info = self.ph.std_title(title, with_ep=True)
                    if title != '':
                        title = info.get('title_display')
                    otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                    params = dict(cItem)
                    params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': otherInfo})
                    self.addDir(params)
            else:
                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
                self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("EgyDead.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        if searchType == 'all':
            url = self.getFullUrl('/?s={}'.format(urllib_quote_plus(searchPattern)))
        elif searchType == 'movies':
            url = self.getFullUrl('/?s=%D9%81%D9%8A%D9%84%D9%85+{}'.format(urllib_quote_plus(searchPattern)))
        elif searchType == 'series':
            url = self.getFullUrl('/?s=%D9%85%D8%B3%D9%84%D8%B3%D9%84+{}'.format(urllib_quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG("EgyDead.getLinksForVideo [%s]" % cItem)
        urlTab = []

        if 'Trailer' in cItem['title']:
            return self.up.getVideoLinkExt(cItem['url'])

        sts, data = self.getPage(cItem['url'], post_data={'View': '1'})
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'serversList'), ('</ul', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, '<li', ('</li', '>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''data-link=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<p', '>'), ('</p', '>'), False)[1])

            if title != '':
                title = ('{} {} [{}]{}{} - {}{}'.format(cItem['title'], E2ColoR('lightred'), title, E2ColoR('white'), E2ColoR('yellow'), self.up.getHostName(url, True), E2ColoR('white')))

            urlTab.append({'name': title, 'url': url, 'need_resolve': 1})

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'donwload-servers-list'), ('</ul', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', '>'), ('</li', '>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<span', '>', 'ser-name'), ('</span', '>'), False)[1])

            if title != '':
                title = ('{} {} [{}]{}{} - {}{}'.format(cItem['title'], E2ColoR('lightred'), title, E2ColoR('white'), E2ColoR('yellow'), self.up.getHostName(url, True), E2ColoR('white')))

            urlTab.append({'name': title, 'url': url, 'need_resolve': 1})

        self.cacheLinks[str(cItem['url'])] = urlTab
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("EgyDead.getVideoLinks [%s]" % videoUrl)

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("EgyDead.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'LeftBox'), ('<div', '>'), True)[1]

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('<a', '>', 'javascript:void(0)'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['duration'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('quality', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['quality'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('year', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['year'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('النوع :', '</span>'), ('</li', '>'), False)[1])
        if Info != '':
            otherInfo['genre'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('country', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['country'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('language', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['language'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('category', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['category'] = Info

        return [{'title': cItem['title'], 'text': cItem['desc'], 'images': [{'title': '', 'url': self.getFullUrl(cItem['icon'])}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

    # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'movei' or category == 'serie' or category == 'other' or category == 'anime' or category == 'tvshow':
            self.listCatItems(self.currItem)
        elif category == 'list_items' or category == 'home':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, EgyDead(), True, [])

    def getSearchTypes(self):
        searchTypesOptions = []
        searchTypesOptions.append(("All", "all"))
        searchTypesOptions.append(("Movies", "movies"))
        searchTypesOptions.append(("Tv Series", "series"))
        return searchTypesOptions

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
