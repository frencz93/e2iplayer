# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from base64 import b64decode
from re import DOTALL, compile

import requests
from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.p2p3.manipulateStrings import ensure_str
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'CimaNow'


class CimaNow(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'cimanow', 'cookie': 'cimanow.cookie'})

        self.MAIN_URL = 'https://bs.cimanow.cc/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/VL1PLZ9/cimanow.png'

        self.HEADER = self.cm.getDefaultHeader('mobile')
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'with_metadata': True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        if 'adilbo_HTML_encoder' in data:
            t_script = compile('''<script.*?;.*?['"](.*?);''', DOTALL).findall(data)
            t_int = compile('/g.....(.*?)\)', DOTALL).findall(data)
            if t_script and t_int:
                script = t_script[0].replace("'", '')
                script = script.replace("+", '')
                script = script.replace("\n", '')
                sc = script.split('.')
                page = ''
                for elm in sc:
                    c_elm = ensure_str(b64decode(elm+'=='))
                    t_ch = compile('\d+', DOTALL).findall(c_elm)
                    if t_ch:
                        nb = int(t_ch[0])+int(t_int[0])
                        page = page + chr(nb)

            data = ensure_str(page, "iso-8859-1")

        return sts, data

    def listMainMenu(self, cItem):
        printDBG("CimaNow.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'home', 'title': _('%s الرئيـــســية' % E2ColoR('lime')), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/home/')},
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/الافلام/')},
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/المسلسلات/')},
            {'category': 'other', 'title': _('رمـــضــان'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/رمضان/')},
            {'category': 'tvshow', 'title': _('بـــرامج'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/برامج-التلفزيونية/')},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG("CimaNow.listCatItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('</main-slider', '>'), ('</body', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<section>', '<span>'), ('</em>', '<a'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''<em[^<].+?href=['"]([^"^']+?)['"][$>]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, '<span>', '<em><a', False)[1])

            if title == '':
                continue

            params = dict(cItem)
            params.update({'category': 'list_items', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': ''})
            self.addDir(params)

    def listItems(self, cItem):
        printDBG("CimaNow.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'pagination'), ('</ul', '>',), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^'^"]+?)['"][^>]*?>%s<''' % (page + 1))[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<section', '>', 'posts'), ('</section', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<article', '>', 'post'), ('</article', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''data-src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<li', '>', 'title'), ('<em', '>'), False)[1])
            desc = self.ph.extract_desc(item, [('quality', '''ribbon['"]>([^>]+?)[$<]'''), ('year', '''year['"].+?([0-9]{4})[$<]'''), ('info', '''tab['"]>([^>]+?)[$<]''')])

            info = self.ph.std_title(title, desc=desc, with_ep=True)
            if title != '':
                title = info.get('title_display')
            desc = info.get('desc')

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("CimaNow.exploreItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        # trailer
        trailerUrl = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'watch'), ('</iframe', '>'))[1]
        trailerUrl = self.cm.ph.getSearchGroups(trailerUrl, '''<iframe src=['"]([^"^']+?)['"]''')[0]
        if self.cm.isValidUrl(trailerUrl):
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': '[%sTrailer%s]' % (E2ColoR('yellow'), E2ColoR('white')), 'url': trailerUrl, 'desc': ''})
            self.addVideo(params)

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('لمحة عامة', '>'), ('</p', '>'), False)[1])

        Season = self.cm.ph.getDataBeetwenMarkers(data, ('<span', '>', 'season-title'), ('</ul', '>'), True)[1]
        if Season:
            self.addMarker({'title': '%s مـــواســم' % E2ColoR('lime'), 'icon': cItem['icon'], 'desc': ''})
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(Season, ('<li', '>'), ('</li', '>'))
            for item in tmp:
                url = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('<em', '>'), False)[1])

                info = self.ph.std_title(title, with_ep=True)
                if title != '':
                    title = info.get('title_display')
                otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                self.addDir(params)

        Episod = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'eps'), ('</ul', '>'), True)[1]
        if Episod:
            self.addMarker({'title': '%s الـحـلـقـات' % E2ColoR('lime'), 'icon': cItem['icon'], 'desc': ''})
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(Episod, ('<li', '>'), ('</li', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getAllItemsBeetwenMarkers(item, 'alt', '/>')[1])

                info = self.ph.std_title(title, with_ep=True)
                if title != '':
                    title = info.get('title_display')
                otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                self.addVideo(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("CimaNow.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        if searchType == 'all':
            url = self.getFullUrl('/?s={}'.format(urllib_quote_plus(searchPattern)))
        elif searchType == 'movies':
            url = self.getFullUrl('/?s=%D9%81%D9%8A%D9%84%D9%85+{}'.format(urllib_quote_plus(searchPattern)))
        elif searchType == 'series':
            url = self.getFullUrl('/?s=%D9%85%D8%B3%D9%84%D8%B3%D9%84+{}'.format(urllib_quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG("CimaNow.getLinksForVideo [%s]" % cItem)
        urlTab = []
        baseUrl = '%swatching/' % cItem['url']

        if 'Trailer' in cItem['title']:
            return self.up.getVideoLinkExt(cItem['url'])

        sts, data = self.getPage(baseUrl)
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'watch'), ('<li', '>', 'embed'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, '<li', ('</li', '>'))
        for item in tmp:
            sIndex = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-index=['"]([^"^']+?)['"]''')[0])
            sId = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-id=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('data-id', '>'), ('</li', '>'), False)[1])

            siteUrl = self.getFullUrl('/wp-content/themes/Cima%20Now%20New/core.php?action=switch&index={}&id={}'.format(sIndex, sId))
            params = {'action': 'switch', 'index': sIndex, 'id': sId}
            hdr = {'User-Agent': self.cm.getDefaultUserAgent('mobile'), 'Referer': self.getMainUrl()}
            St = requests.Session()
            sHtmlContent = St.get(siteUrl, headers=hdr, params=params).content

            url = self.getFullUrl(self.cm.ph.getSearchGroups(sHtmlContent, '''src=['"]([^"^']+?)['"]''', ignoreCase=True)[0])

            if title != '':
                title = ('{} {} [{}]{}{} - {}{}'.format(cItem['title'], E2ColoR('lightred'), title, E2ColoR('white'), E2ColoR('yellow'), self.up.getHostName(url, True), E2ColoR('white')))

            urlTab.append({'name': title, 'url': strwithmeta(url, {'Referer': self.getMainUrl()}), 'need_resolve': 1})

        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("CimaNow.getVideoLinks [%s]" % videoUrl)

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("CimaNow.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'details'), ('</ul', '>'), True)[1]

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('runtime', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['duration'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('quality', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['quality'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('release-year', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['year'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, '<li><a>', '<li> <a', False)[1])
        if Info != '':
            otherInfo['genre'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('المحتوي', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['category'] = Info

        return [{'title': cItem['title'], 'text': cItem['desc'], 'images': [{'title': '', 'url': self.getFullUrl(cItem['icon'])}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

    # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'home' or category == 'movei' or category == 'serie' or category == 'other' or category == 'tvshow':
            self.listCatItems(self.currItem)
        elif category == 'list_items':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, CimaNow(), True, [])

    def getSearchTypes(self):
        searchTypesOptions = []
        searchTypesOptions.append(("All", "all"))
        searchTypesOptions.append(("Movies", "movies"))
        searchTypesOptions.append(("Tv Series", "series"))
        return searchTypesOptions

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
