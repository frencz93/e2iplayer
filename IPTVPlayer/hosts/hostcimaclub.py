# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'CimaClub'


class CimaClub(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'cimaclub', 'cookie': 'cimaclub.cookie'})

        self.MAIN_URL = 'https://cimaclub.watch/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/b3mQWFp/cimaclub.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest', 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7'})
        self.defaultParams = {'header': self.HEADER, 'with_metadata': True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("CimaClub.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'part1', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'part2', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'part3', 'title': _('أنــمـــي'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'part4', 'title': _('بـــرامج'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG("CimaClub.listCatItems cItem[%s]" % (cItem))
        category = self.currItem.get("category", '')

        if category == 'part4':
            NEW_CAT_TAB = [{'category': 'list_items', 'title': _('مصارعة حرة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مصارعة-حرة/')}]
            self.listsTab(NEW_CAT_TAB, cItem)
        else:
            if category == 'part1':
                sStart = '>أفلام<'
            elif category == 'part2':
                sStart = '>مسلسلات<'
            elif category == 'part3':
                sStart = '>انمى وكرتون<'
            sts, data = self.getPage(self.getMainUrl())
            if not sts:
                return

            tmp = self.cm.ph.getDataBeetwenMarkers(data, sStart, ('</ul', '>'), True)[1]
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, '<li', ('</li', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, '>', '</a>', False)[1])

                params = dict(cItem)
                params.update({'category': 'list_items', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': ''})
                self.addDir(params)

    def listItems(self, cItem):
        printDBG("CimaClub.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'paginate'), ('</ul>', '</div>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^'^"]+?)['"][^>]*?>%s<''' % (page + 1))[0])

        titlesTab = []
        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'container page-content'), ('<div', '>', 'navigation-menu'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'content-box'), ('</h3>', '</a>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<h3', '>'), ('</h3', '>'), False)[1])

            info = self.ph.std_title(title)
            if title != '':
                title = info.get('title_display')
            desc = info.get('desc')

            if title not in titlesTab:
                titlesTab.append(title)
                params = dict(cItem)
                params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
                self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("CimaClub.exploreItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        # trailer
        trailerUrl = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'ShowTrailerSingle'), ('</div', '>'))[1]
        trailerUrl = self.cm.ph.getSearchGroups(trailerUrl, '''href=['"]([^"^']+?)['"]''')[0]
        if self.cm.isValidUrl(trailerUrl):
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': '[%sTrailer%s]' % (E2ColoR('yellow'), E2ColoR('white')), 'url': trailerUrl, 'desc': ''})
            self.addVideo(params)

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('>قصة العرض<', '<p>'), ('</p', '>'), False)[1])

        Episod = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'series-episode'), ('</ul>', '</div>'), True)[1]
        if Episod:
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(Episod, ('<a', '>'), ('</a', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</a', '>'), False)[1])

                info = self.ph.std_title(title, with_ep=True)
                if title != '':
                    title = info.get('title_display')
                otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                self.addVideo(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("CimaClub.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        url = self.getFullUrl('/?s={}'.format(urllib_quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG("CimaClub.getLinksForVideo [%s]" % cItem)
        urlTab = []

        if 'Trailer' in cItem['title']:
            return self.up.getVideoLinkExt(cItem['url'])

        params = dict(self.defaultParams)
        params['header'] = dict(self.AJAX_HEADER)
        params['header']['Referer'] = self.up.getDomain(self.getMainUrl(), True)

        baseUrl = '{}see/'.format(cItem['url'])
        sts, data = self.getPage(baseUrl, params)
        if not sts:
            return
        cUrl = data.meta['url']

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'tabContainer'), ('</cente', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', '>'), ('</li', '>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''data-embed=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</a', '>'), False)[1])

            if title != '':
                title = ('{} {} [{}]{}{} - {}{}'.format(cItem['title'], E2ColoR('lightred'), title, E2ColoR('white'), E2ColoR('yellow'), self.up.getHostName(url, True), E2ColoR('white')))
            urlTab.append({'name': title, 'url': strwithmeta(url, {'Referer': cUrl}), 'need_resolve': 1})
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("CimaClub.getVideoLinks [%s]" % videoUrl)

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("CimaClub.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'tax-list media-p'), ('<div', '>', 'shortlink'), True)[1]

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('fa fa-star', '<span>'), ('</span', '>'), False)[1])
        if Info != '':
            otherInfo['imdb_rating'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('genre', '>'), ('</ul', '>'), False)[1])
        if Info != '':
            otherInfo['genres'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('language', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['language'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('release-year', '<span>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['released'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('nation', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['country'] = Info

        return [{'title': cItem['title'], 'text': cItem['desc'], 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

        # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'part1' or category == 'part2' or category == 'part3' or category == 'part4':
            self.listCatItems(self.currItem)
        elif category == 'list_items':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, CimaClub(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
