# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'Shahid4u'


class Shahid4u(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'shahee4u', 'cookie': 'shahee4u.cookie'})

        self.MAIN_URL = 'https://she4u.bet/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/pb6W99R/shahidu.png'

        self.HEADER = self.cm.getDefaultHeader('firefox')
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("Shahid4u.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'part0', 'title': _('%s الرئيـــســية' % E2ColoR('lime')), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'part1', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'part2', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'part3', 'title': _('بـرامــــج'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG("Shahid4u.listCatItems cItem[%s]" % (cItem))
        category = self.currItem.get("category", '')

        if category == 'part0':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('المثبت'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/?order=pin_index')},
                {'category': 'list_items', 'title': _('الاكثر مشاهدة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/?order=views')},
                {'category': 'list_items', 'title': _('الأعلي تقييماََ'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/?order=rating')},
                {'category': 'list_items', 'title': _('جـديـد الأفـلام'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/?order=last_films')},
                {'category': 'list_items', 'title': _('جـديد الحـلـقات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/?order=last_eps')},]
        elif category == 'part1':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('افلام اجنبي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/افلام-اجنبي/')},
                {'category': 'list_items', 'title': _('افلام انمي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/افلام-انمي/')},
                {'category': 'list_items', 'title': _('افلام اسيوية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/افلام-اسيوية/')},
                {'category': 'list_items', 'title': _('افلام هندي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/افلام-هندي/')},
                {'category': 'list_items', 'title': _('افلام عربي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/افلام-عربي/')},
                {'category': 'list_items', 'title': _('افلام تركية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/افلام-تركية/')}]
        elif category == 'part2':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('مسلسلات رمضان 2024'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-رمضان-2024')},
                {'category': 'list_items', 'title': _('مسلسلات اجنبي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-اجنبي/')},
                {'category': 'list_items', 'title': _('مسلسلات انمي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-انمي/')},
                {'category': 'list_items', 'title': _('مسلسلات تركية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-تركية/')},
                {'category': 'list_items', 'title': _('مسلسلات عربي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-عربي/')},
                {'category': 'list_items', 'title': _('مسلسلات اسيوية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-اسيوية/')},
                {'category': 'list_items', 'title': _('مسلسلات هندية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-هندية/')}]
        elif category == 'part3':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('برامج تلفزيونية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/برامج-تلفزيونية')},
                {'category': 'list_items', 'title': _('عروض مصارعة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/عروض-مصارعة')}]
        self.listsTab(NEW_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG("Shahid4u.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)
        pageUrl = cItem['url']

        if page > 1:
            pageUrl.replace('?page=', '&page=')

        sts, data = self.getPage(pageUrl)
        if not sts:
            return
        cUrl = self.cm.meta['url']

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'pagination'), ('</ul>', '</nav>'), True)[1]

        titlesTab = []
        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'shows-container row'), ('<script', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'col-6 col-md-4 col-lg-20ps mb-3'), ('</a>', '</div>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''url\((.+?)\)''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<h4', '>', 'title'), ('</h4', '>'), False)[1])
            rating = self.ph.extract_desc(item, [('rating', '''rate['"]>([^>]+?)\s''')])
            desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<h5', '>', 'description'), ('</h5', '>'), False)[1])

            info = self.ph.std_title(title.split('الحلقة')[0], desc=rating)
            if title != '':
                title = info.get('title_display')
            otherInfo = '{}\n{}'.format(info.get('desc'), desc)

            if title not in titlesTab:
                titlesTab.append(title)
                params = dict(cItem)
                params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': otherInfo})
                self.addDir(params)

        if nextPage != '':
            nextPage = '%s?page=%s' % (cUrl, page + 1)
            if '?page=' in cUrl:
                nextPage = '%s?page=%s' % (cUrl.split('?page=')[0], page + 1)

            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("Shahid4u.exploreItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<span', '>', 'description'), ('</span', '>'), False)[1])
        if 'episode' in cItem['url']:
            if "المواسم" in data:
                self.addMarker({'title': '%s مـــواســم' % E2ColoR('lime'), 'icon': cItem['icon'], 'desc': ''})
                tmp = self.cm.ph.getDataBeetwenMarkers(data, 'جميع المواسم', 'class="glide__arrows"', True)[1]
                tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, '<a', ('</a', '>'))
                for item in tmp:
                    url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                    title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<span', '>'), ('</h3', '>'), False)[1])

                    info = self.ph.std_title(title, with_ep=True)
                    if title != '':
                        title = info.get('title_display')
                    otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                    params = dict(cItem)
                    params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                    self.addDir(params)
            if "الحلقات" in data:
                self.addMarker({'title': '%s الـحـلـقـات' % E2ColoR('lime'), 'icon': cItem['icon'], 'desc': ''})
                tmp = self.cm.ph.getDataBeetwenMarkers(data, 'جميع الحلقات', 'جميع المواسم', True)[1]
                tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, '<a', ('</a', '>'))
                for item in tmp:
                    url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                    title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<span', '>'), ('</h3', '>'), False)[1])

                    info = self.ph.std_title(title, with_ep=True)
                    if title != '':
                        title = info.get('title_display')
                    otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                    params = dict(cItem)
                    params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                    self.addVideo(params)
        elif 'season' in cItem['url']:
            baseUrl = '%s/episodes' % cItem['url']
            sts, data = self.getPage(baseUrl)
            if not sts:
                return
            tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'shows-container row'), ('<script', '>'), True)[1]
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'col-6 col-md-4 col-lg-20ps mb-3'), ('</a>', '</div>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<h4', '>', 'title'), ('</h4', '>'), False)[1])

                info = self.ph.std_title(title, with_ep=True)
                if title != '':
                    title = info.get('title_display')
                otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                self.addVideo(params)
        elif "film" in cItem['url'] or "post" in cItem['url']:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("Shahid4u.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        if searchType == 'all':
            url = self.getFullUrl('/search?s={}'.format(urllib_quote_plus(searchPattern)))
        elif searchType == 'movies':
            url = self.getFullUrl('/search?s=%D9%81%D9%8A%D9%84%D9%85+{}'.format(urllib_quote_plus(searchPattern)))
        elif searchType == 'series':
            url = self.getFullUrl('/search?s=%D9%85%D8%B3%D9%84%D8%B3%D9%84+{}'.format(urllib_quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG("Shahid4u.getLinksForVideo [%s]" % cItem)
        urlTab = []

        baseUrl = cItem['url'].replace("film", "watch").replace("episode", "watch").replace("post", "watch")
        sts, data = self.getPage(baseUrl)
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(self.ph.decodeHtml(data), 'JSON.parse', '#player', True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, 'id', 'rank')
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''url['"]:['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''name['"]:['"]([^"^']+?)['"]''')[0])

            if title != '':
                title = ('{} {} [{}]{}{} - {}{}'.format(cItem['title'], E2ColoR('lightred'), title, E2ColoR('white'), E2ColoR('yellow'), self.up.getHostName(url, True), E2ColoR('white')))
            urlTab.append({'name': title, 'url': url, 'need_resolve': 1})
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("Shahid4u.getVideoLinks [%s]" % videoUrl)

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("Shahid4u.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'info-side col-12 col-md-8'), ('<hr', '>', 'my-4 text-white'), True)[1]

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<span', '>', 'description'), ('</span', '>'), False)[1])
        if desc == '':
            desc = cItem['desc']

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('release-year', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['year'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('quality', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['quality'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('genre', '>'), ('</div', '>'), False)[1])
        if Info != '':
            otherInfo['genre'] = Info

        return [{'title': cItem['title'], 'text': desc, 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

        # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'part0' or category == 'part1' or category == 'part2' or category == 'part3':
            self.listCatItems(self.currItem)
        elif category == 'list_items':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, Shahid4u(), True, [])

    def getSearchTypes(self):
        searchTypesOptions = []
        searchTypesOptions.append(("All", "all"))
        searchTypesOptions.append(("Movies", "movies"))
        searchTypesOptions.append(("Tv Series", "series"))
        return searchTypesOptions

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
