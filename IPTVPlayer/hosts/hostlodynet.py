# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'LodyNet'


class LodyNet(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'lodynet', 'cookie': 'lodynet.cookie'})

        self.MAIN_URL = 'https://llodynet.site/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/nDjyfNg/lodynet.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.cacheLinks = {}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("LodyNet.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG("LodyNet.listCatItems cItem[%s]" % (cItem))
        category = self.currItem.get("category", '')

        if category == 'movei':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('أفــلام أجنـبيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/افلام-اجنبية-مترجمة-a/')},
                {'category': 'list_items', 'title': _('أفــلام هنــديـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/افلام-هندية/')},
                {'category': 'list_items', 'title': _('أفــلام آسـيـويـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/افلام-اسيوية-a/')},
                {'category': 'list_items', 'title': _('أفــلام تـركـيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/افلام-تركية-مترجم/')},
                {'category': 'list_items', 'title': _('أفــلام أنـمـي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/انيمي/')},]
        elif category == 'serie':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('مسـلـسـلات آسـيـويـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/tag/new-asia/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات تـركـيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/turkish-series-2b/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات تـركـيـة مدبلجة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/dubbed-turkish-series-g/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات هنــديـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/bالمسلسلات-هندية-مترجمة/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات هنــديـة مدبلجة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/dubbed-indian-series-p4/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات باكسـتـانـية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/المسلسلات-باكستانية/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات لاتــنـية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-مكسيكية-a/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات صيـنـية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-صينية-مترجمة/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات تايلنـديـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/مشاهدة-مسلسلات-تايلندية/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات كــوريـــة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/korean-series-a/')},]
        self.listsTab(NEW_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG("LodyNet.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'pagination'), ('</ul', '>',), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^'^"]+?)['"][^>]*?>%s<''' % (page + 1))[0])

        titlesTab = []
        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'BlocksArea'), ('<script', '>', 'javascript'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', '>', 'LodyBlock'), ('</li', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''alt=['"]([^"^']+?)['"]''')[0])
            desc = self.ph.extract_desc(item, [('quality', '''Ribbon['"].+?([^>]+?)[$<]''')])

            info = self.ph.std_title(title, desc=desc)
            if title != '':
                title = info.get('title_display')
            desc = info.get('desc')

            if title not in titlesTab:
                titlesTab.append(title)
                params = dict(cItem)
                params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
                self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("LodyNet.exploreItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('DetailsBoxContentInner', '<br />'), ('</p', '>'), False)[1])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<i', '>', 'fal fa-retweet'), ('<script', '>', 'javascript'), True)[1]
        if tmp:
            nextPage = self.cm.ph.getDataBeetwenMarkers(tmp, ('<div', '>', 'pagination'), ('</ul', '>',), True)[1]
            nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^'^"]+?)['"][^>]*?>%s<''' % (page + 1))[0])

            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', '>', 'LodyBlock'), ('</li', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''alt=['"]([^"^']+?)['"]''')[0])

                info = self.ph.std_title(title, with_ep=True)
                if title != '':
                    title = info.get('title_display')
                otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                params = dict(cItem)
                params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                self.addVideo(params)

            if nextPage != '':
                params = dict(cItem)
                params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
                self.addMore(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("LodyNet.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        url = self.getFullUrl('/search/{}'.format(urllib_quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG("LodyNet.getLinksForVideo [%s]" % cItem)
        urlTab = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'ServersList'), ('</ul', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, '<li', ('</li', '>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''data-embed=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(item)

            if title != '':
                title = ('{} {} [{}]{}{} - {}{}'.format(cItem['title'], E2ColoR('lightred'), title, E2ColoR('white'), E2ColoR('yellow'), self.up.getHostName(url, True), E2ColoR('white')))

            urlTab.append({'name': title, 'url': url, 'need_resolve': 1})

        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("LodyNet.getVideoLinks [%s]" % videoUrl)

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("LodyNet.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('DetailsBoxContentInner', '<br />'), ('</p', '>'), False)[1])
        if desc == '':
            desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('DetailsBoxContentInner', '</p>'), '</p>', False)[1])

        return [{'title': cItem['title'], 'text': desc, 'images': [{'title': '', 'url': self.getFullUrl(cItem['icon'])}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

    # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'movei' or category == 'serie':
            self.listCatItems(self.currItem)
        elif category == 'list_items':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, LodyNet(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
