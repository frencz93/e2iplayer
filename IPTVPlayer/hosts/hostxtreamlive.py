# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS

from base64 import b64decode
from os import listdir
from re import DOTALL, findall

from Components.config import ConfigText, config, getConfigListEntry
from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    GetIPTVNotify
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs.e2ijson import loads as json_loads
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (E2ColoR, printDBG,
                                                           printExc)
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
from Tools.Directories import (SCOPE_CONFIG, SCOPE_SYSETC, fileExists,
                               resolveFilename)

try:
    import json
except Exception:
    import simplejson as json


config.plugins.iptvplayer.ts_xtream_user = ConfigText(default='', fixed_size=False)
config.plugins.iptvplayer.ts_xtream_pass = ConfigText(default='', fixed_size=False)
config.plugins.iptvplayer.ts_xtream_host = ConfigText(default='', fixed_size=False)
config.plugins.iptvplayer.ts_xtream_ua = ConfigText(default='', fixed_size=False)


def GetConfigList():
    optionList = []
    optionList.append(getConfigListEntry(_("    Xtream User:"), config.plugins.iptvplayer.ts_xtream_user))
    optionList.append(getConfigListEntry(_("    Xtream Pass:"), config.plugins.iptvplayer.ts_xtream_pass))
    optionList.append(getConfigListEntry(_("    Xtream Host:"), config.plugins.iptvplayer.ts_xtream_host))
    optionList.append(getConfigListEntry(_("    Xtream User Agent:"), config.plugins.iptvplayer.ts_xtream_ua))
    return optionList


def gettytul():
    return 'Xtream %sLIVE%s' % (E2ColoR('cyan'), E2ColoR('white'))


class XtreamLive(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'cookie': 'xtreamlive.cookie'})

        self.MAIN_URL = ''
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/LYGhL5v/xtream.png'

        self.USER_AGENT = 'Mozilla/5.0 (Linux; Android 4.4.2; SAMSUNG-SM-N900A Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Safari/537.36'
        self.HEADER = {'User-Agent': self.USER_AGENT, 'X-Requested-With': 'com.sportstv20.app', 'Referer': self.getMainUrl(), 'Origin': self.getMainUrl()}
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def getXtreamConf(self):
        multi_tab = []
        User = config.plugins.iptvplayer.ts_xtream_user.value
        Pass = config.plugins.iptvplayer.ts_xtream_pass.value
        Host = config.plugins.iptvplayer.ts_xtream_host.value
        xUa = config.plugins.iptvplayer.ts_xtream_ua.value
        if ((User != '') and (Pass != '') and (Host != '')):
            name_ = '%s (%s)' % (Host, User)
            if not Host.startswith('http'):
                Host = 'http://%s' % Host
            multi_tab.append((name_, Host, User, Pass, xUa))

        if fileExists(resolveFilename(SCOPE_SYSETC, 'tsiplayer_xtream.conf')):
            with open(resolveFilename(SCOPE_SYSETC, 'tsiplayer_xtream.conf')) as f:
                for line in f:
                    line = line.strip()
                    name_, ua_, host_, user_, pass_ = '', '', '', '', ''
                    _data = findall('(.*?//.*?)/.*?username=(.*?)&.*?password=(.*?)&', line, DOTALL)
                    if _data:
                        name_, host_, user_, pass_ = _data[0][0]+' ('+_data[0][1]+')', _data[0][0], _data[0][1], _data[0][2]
                    else:
                        _data = findall('(.*?)#(.*?)#(.*?)#(.*?)#(.*)', line, DOTALL)
                        if _data:
                            name_, host_, user_, pass_, ua_ = _data[0][0], _data[0][1], _data[0][2], _data[0][3], _data[0][4]
                        else:
                            _data = findall('(.*?)#(.*?)#(.*?)#(.*)', line, DOTALL)
                            if _data:
                                name_, host_, user_, pass_ = _data[0][0], _data[0][1], _data[0][2], _data[0][3]
                    if ((user_ != '') and (pass_ != '') and (host_ != '')):
                        if not host_.startswith('http'):
                            host_ = 'http://'+host_
                        multi_tab.append((name_, host_, user_, pass_, ua_))
        return multi_tab

    def listMainMenu(self, cItem):
        printDBG("XtreamLive.listMainMenu")
        multi_tab = self.getXtreamConf()
        if len(multi_tab) == 0:
            self.addMarker({'title': 'Please configure xstream first', 'icon': self.DEFAULT_ICON_URL, 'desc': 'Please configure xstream first, (add user, pass, host in E2Iplayer params or add your config file in /etc/tsiplayer_xtream.conf)'})
        elif len(multi_tab) == 1:
            params = dict(cItem)
            params.update({'icon': self.DEFAULT_ICON_URL, 'xuser': multi_tab[0][2], 'xpass': multi_tab[0][3], 'xhost': multi_tab[0][1], 'xua': multi_tab[0][4]})
            self.listItems(params)
        else:
            for item in multi_tab:
                params = dict(cItem)
                params.update({'category': 'listCatItems', 'title': item[0], 'icon': self.DEFAULT_ICON_URL, 'xuser': item[2], 'xpass': item[3], 'xhost': item[1], 'xua': item[4]})
                self.addDir(params)

    def listCatItems(self, cItem):
        printDBG("XtreamLive.listCatItems cItem[%s]" % (cItem))

        Url = '%s/player_api.php?username=%s&password=%s&action=get_live_categories' % (cItem['xhost'], cItem['xuser'], cItem['xpass'])

        sts, data = self.getPage(Url)
        if not sts:
            return

        try:
            tmp = json_loads(data)
            params = dict(cItem)
            params.update({'category': 'listItems', 'title': 'All', 'icon': cItem['icon'], 'category_id': '', 'xuser': cItem['xuser'], 'xpass': cItem['xpass'], 'xhost': cItem['xhost'], 'xua': cItem['xua']})
            self.addDir(params)
            for item in tmp:
                params = dict(cItem)
                params.update({
                    'good_for_fav': True, 'EPG': True, 'category': 'listItems', 'title': item['category_name'].strip(), 'icon': cItem['icon'], 'category_id': item['category_id'], 'xuser': cItem['xuser'], 'xpass': cItem['xpass'], 'xhost': cItem['xhost'], 'xua': cItem['xua']})
                self.addDir(params)
        except Exception:
            printExc('Cannot parse received data !')

    def listItems(self, cItem):
        printDBG("XtreamLive.listItems cItem[%s]" % (cItem))

        Url = '%s/player_api.php?username=%s&password=%s&action=get_live_categories' % (cItem['xhost'], cItem['xuser'], cItem['xpass'])

        sts, data = self.getPage(Url)
        if not sts:
            return

        tmp = json_loads(data)
        params = dict(cItem)
        params.update({'category': 'listItems', 'title': 'All', 'icon': cItem['icon'], 'category_id': '', 'xuser': cItem['xuser'], 'xpass': cItem['xpass'], 'xhost': cItem['xhost'], 'xua': cItem['xua']})
        self.addDir(params)
        for item in tmp:
            params = dict(cItem)
            params.update({'category': 'listItems', 'title': item['category_name'].strip(), 'icon': cItem['icon'], 'category_id': item['category_id'], 'xuser': cItem['xuser'], 'xpass': cItem['xpass'], 'xhost': cItem['xhost'], 'xua': cItem['xua']})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("XtreamLive.exploreItems cItem[%s]" % (cItem))

        Url = '%s/player_api.php?username=%s&password=%s&action=get_live_streams&category_id=%s' % (cItem['xhost'], cItem['xuser'], cItem['xpass'], str(cItem['category_id']))

        sts, data = self.getPage(Url)
        if not sts:
            return
        tmp = json_loads(data)
        for item in tmp:
            tmpUrl = '%s/live/%s/%s/%s.ts' % (cItem['xhost'], cItem['xuser'], cItem['xpass'], str(item['stream_id']))
            if cItem['xua'] != '':
                tmpUrl = strwithmeta(tmpUrl, {'User-Agent': cItem['xua']})
            if item['stream_icon']:
                stream_icon = item['stream_icon']
            else:
                stream_icon = ''
            if '---' in item['name']:
                self.addMarker({'title': '%s%s' % (E2ColoR('lime'), item['name']), 'icon': cItem['icon']})
            elif '***' in item['name']:
                self.addMarker({'title': '%s%s' % (E2ColoR('yellow'), item['name']), 'icon': cItem['icon']})
            else:
                opt = False
                if (fileExists(resolveFilename(SCOPE_CONFIG, 'ipaudio.json')) or
                        fileExists(resolveFilename(SCOPE_CONFIG, 'IPAudioPro.json')) or
                        fileExists(resolveFilename(SCOPE_CONFIG, 'ipaudioplus_user_list.json')) or
                        fileExists(resolveFilename(SCOPE_CONFIG, 'iptosat.json'))):
                    opt = True
                for file in listdir("/etc/enigma2"):
                    if ((file.endswith(".tv")) and ('tsiplayer' in file.lower())):
                        opt = True
                        break
                if opt:
                    params = dict(cItem)
                    params.update({'category': 'listOptions', 'url': tmpUrl, 'title': item['name'], 'icon': stream_icon, 'xuser': cItem['xuser'], 'xpass': cItem['xpass'], 'xhost': cItem['xhost'], 'stream_id': item['stream_id']})
                    self.addDir(params)
                else:
                    params = dict(cItem)
                    params.update({'good_for_fav': True, 'EPG': True, 'url': tmpUrl, 'title': item['name'], 'icon': stream_icon, 'xuser': cItem['xuser'], 'xpass': cItem['xpass'], 'xhost': cItem['xhost'], 'stream_id': item['stream_id']})
                    self.addVideo(params)

    def ShowOptions(self, cItem):
        printDBG("XtreamLive.ShowOptions cItem[%s]" % (cItem))
        params = dict(cItem)
        params.update({'good_for_fav': True, 'EPG': True, 'url': cItem['url'], 'title': cItem['title'], 'icon': cItem['icon'], 'xuser': cItem['xuser'], 'xpass': cItem['xpass'], 'xhost': cItem['xhost'], 'stream_id': cItem['stream_id']})
        self.addVideo(params)

        Paths = {'ipaudio': 'ipaudio.json', 'ipaudiopro': 'IPAudioPro.json', 'ipaudioplus': 'ipaudioplus_user_list.json', 'iptosat': 'iptosat.json'}
        for category, defaultPaths in Paths.items():
            if fileExists(resolveFilename(SCOPE_CONFIG, defaultPaths)):
                params = dict(cItem)
                params.update({'category': category, 'url': cItem['url'], 'title': 'Add to %s' % category.capitalize(), 'titre': cItem['title'], 'icon': cItem['icon']})
                self.addDir(params)

    def addtoipaudio(self, cItem):
        try:
            params = {"channel": cItem['titre'], "url": cItem['url']}
            with open(resolveFilename(SCOPE_CONFIG, 'ipaudio.json'), 'r')as f:
                playlist = json.loads(f.read())
            playlist['playlist'].append(params)
            with open(resolveFilename(SCOPE_CONFIG, 'ipaudio.json'), 'w') as f:
                json.dump(playlist, f, indent=4)

            GetIPTVNotify().push('#%s# Successfully added' % cItem['titre'], 'info', 5)
            self.exploreItems(self.currItem)
        except Exception:
            GetIPTVNotify().push('#%s# Not added (Error)' % cItem['titre'], 'error', 5)
            self.exploreItems(self.currItem)

    def addtoipaudiopro(self, cItem):
        try:
            params = {"name": cItem['titre'], "display_name": cItem['titre'], "url": cItem['url']}
            with open(resolveFilename(SCOPE_CONFIG, 'IPAudioPro.json'), 'r')as f:
                streams = json.loads(f.read())
            streams['streams'].append(params)
            with open(resolveFilename(SCOPE_CONFIG, 'IPAudioPro.json'), 'w') as f:
                json.dump(streams, f, indent=4)

            GetIPTVNotify().push('#%s# Successfully added' % cItem['titre'], 'info', 5)
            self.exploreItems(self.currItem)
        except Exception:
            GetIPTVNotify().push('#%s# Not added (Error)' % cItem['titre'], 'error', 5)
            self.exploreItems(self.currItem)

    def addtoipaudioplus(self, cItem):
        try:
            params = {"Name": cItem['titre'], "Url": cItem['url']}
            with open(resolveFilename(SCOPE_CONFIG, 'ipaudioplus_user_list.json'), 'r')as f:
                playlist = json.loads(f.read())
                params.update({"Id": str(int(playlist['User List']["channels"][-1]['Id'])+1)})
            playlist['User List']["channels"].append(params)
            with open(resolveFilename(SCOPE_CONFIG, 'ipaudioplus_user_list.json'), 'w') as f:
                json.dump(playlist, f, indent=4)

            GetIPTVNotify().push('#%s# Successfully added' % cItem['titre'], 'info', 5)
            self.exploreItems(self.currItem)
        except Exception:
            GetIPTVNotify().push('#%s# Not added (Error)' % cItem['titre'], 'error', 5)
            self.exploreItems(self.currItem)

    def addtoiptosat(self, cItem):
        try:
            params = {"channel": cItem['titre'], "url": cItem['url']}
            with open(resolveFilename(SCOPE_CONFIG, 'iptosat.json'), 'r')as f:
                playlist = json.loads(f.read())
            playlist['playlist'].append(params)
            with open(resolveFilename(SCOPE_CONFIG, 'iptosat.json'), 'w') as f:
                json.dump(playlist, f, indent=4)

            GetIPTVNotify().push('#%s# Successfully added' % cItem['titre'], 'info', 5)
            self.exploreItems(self.currItem)
        except Exception:
            GetIPTVNotify().push('#%s# Not added (Error)' % cItem['titre'], 'error', 5)
            self.exploreItems(self.currItem)

    def getLinksForVideo(self, cItem):
        printDBG("XtreamLive.getLinksForVideo [%s]" % cItem)
        urlTab = []

        urlTab.append({'name': '', 'url': strwithmeta(cItem['url'], {'User-Agent': self.USER_AGENT}), 'need_resolve': 0})

        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("XtreamLive.getVideoLinks [%s]" % videoUrl)

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("XtreamLive.getArticleContent [%s]" % cItem)
        otherInfo = {}

        Url = '%s/player_api.php?username=%s&password=%s&action=get_short_epg&stream_id=%s' % (cItem['xhost'], cItem['xuser'], cItem['xpass'], cItem['stream_id'])
        sts, data = self.getPage(Url)
        if not sts:
            return

        try:
            tmp = json_loads(data)
            for item in tmp['epg_listings']:
                time_star = item['start'].split(' ')[1]
                time_end = item['end'].split(' ')[1]
                time1, time2, x1 = time_star.split(':')
                time_1, time_2, x1 = time_end.split(':')
                start_ = '%s[%s:%s-%s:%s]%s' % (E2ColoR('lime'), time1, time2, time_1, time_2, E2ColoR('white'))
                title = b64decode(item['title'])
                descr = b64decode(item['description'])
                desc = '%s%s | %s | %s\\n' % (desc, start_, title, descr)

            return [{'title': cItem['title'], 'text': desc, 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]
        except Exception:
            GetIPTVNotify().push('Info not available ...!', 'error', 5)

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

    # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'listCatItems':
            self.listCatItems(self.currItem)
        elif category == 'list_items':
            self.listItems(self.currItem)
        elif category == 'listOptions':
            self.ShowOptions(self.currItem)
        elif category == 'ipaudio':
            self.addtoipaudio(self.currItem)
        elif category == 'ipaudiopro':
            self.addtoipaudiopro(self.currItem)
        elif category == 'ipaudioplus':
            self.addtoipaudioplus(self.currItem)
        elif category == 'iptosat':
            self.addtoiptosat(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, XtreamLive(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
