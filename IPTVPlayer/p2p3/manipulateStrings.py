# macro to load functions from correct modules depending on the python version
# some functions copied from six library to support old nbox python - al credits go to it authors.
# build to simplify loading modules in e2iplayer scripts
# just change:
#   from urlib import
# to:
#   from Plugins.Extensions.IPTVPlayer.p2p3.manipulateStrings import
#


def strDecode(text,  setErrors='strict'):
    if isinstance(text, str):
        retVal = text
    else:
        try:
            retVal = text.decode(encoding='utf-8', errors=setErrors)
        except Exception:
            retVal = text.decode(encoding='utf-8', errors='ignore')
    return retVal


def iterDictItems(myDict):
    return myDict.items()


def iterDictKeys(myDict):
    return myDict.keys()


def iterDictValues(myDict):
    return myDict.values()


def strEncode(text,  encoding='utf-8'):
    retVal = text.encode(encoding)
    return retVal


def ensure_binary(text, encoding='utf-8', errors='strict'):  # based on six library
    if isinstance(text, bytes):
        return text
    if isinstance(text, str):
        try:
            return text.encode(encoding, errors)
        except Exception:
            return text.encode(encoding, 'ignore')
    return text

def ensure_str(text, encoding='utf-8', errors='strict'): #based on six library
    if type(text) is str:
        return text
    if isinstance(text, bytes):
        try:
            return text.decode(encoding, errors)
        except Exception:
            return text.decode(encoding, 'ignore')
    return text # strwithmeta type defined in e2iplayer goes through it

def escape_str(s, encoding='utf-8'):
    return str(s.encode().decode('unicode-escape'))