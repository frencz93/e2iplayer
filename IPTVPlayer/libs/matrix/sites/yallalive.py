﻿# -*- coding: utf-8 -*-
# zombi https://github.com/zombiB/zombi-addons/

import base64
import re

from Plugins.Extensions.IPTVPlayer.libs.matrix.lib import random_ua
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import (
    addon, isMatrix, siteManager)
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.gui.gui import cGui
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.gui.hoster import cHosterGui
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.inputParameterHandler import \
    cInputParameterHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.outputParameterHandler import \
    cOutputParameterHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.requestHandler import \
    cRequestHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.packer import cPacker
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.util import Quote

SITE_IDENTIFIER = 'yallalive'
SITE_NAME = 'Yallalive'
SITE_DESC = 'arabic vod'

URL_MAIN = siteManager().getUrlMain(SITE_IDENTIFIER)

SPORT_LIVE = (URL_MAIN, 'showMovies')

UA = random_ua.get_ua()


def load():
    oGui = cGui()
    addons = addon()
    oOutputParameterHandler = cOutputParameterHandler()

    oGui.addText(SITE_IDENTIFIER, '[COLOR olive]-----●★| ' + addons.VSlang(30350)+' |★●-----[/COLOR]')
    oOutputParameterHandler.addParameter('siteUrl', SPORT_LIVE[0])
    oGui.addDir(SITE_IDENTIFIER, 'showMovies', 'بث مباشر', 'sport.png', oOutputParameterHandler)

    oGui.setEndOfDirectory()


def showMovies():
    oGui = cGui()
    oParser = cParser()

    oInputParameterHandler = cInputParameterHandler()
    sUrl = oInputParameterHandler.getValue('siteUrl')

    oRequestHandler = cRequestHandler(sUrl)
    sHtmlContent = oRequestHandler.request()

    sStart = '<div id="today"'
    sEnd = '<div id="tommorw"'
    sHtmlContent = oParser.abParse(sHtmlContent, sStart, sEnd)

    sPattern = '<div class="AF_Match.+?class="AF_TeamName.+?>(.+?)</div>.+?class="AF_EvTime">(.+?)</div>.+?<div class="AF_TeamName.+?>(.+?)</div>.+?href="([^"]+)".+?<div class="AF_MaskText">(.+?)</div>'
    aResult = oParser.parse(sHtmlContent, sPattern)
    if aResult[0]:
        oOutputParameterHandler = cOutputParameterHandler()
        for aEntry in aResult[1]:

            sTitle = aEntry[0] + ' - ' + aEntry[2]
            sThumb = ""
            siteUrl = aEntry[3]
            sDesc = aEntry[1] + " KSA \n \n"+aEntry[4]

            oOutputParameterHandler.addParameter('siteUrl', siteUrl)
            oOutputParameterHandler.addParameter('sMovieTitle', sTitle)
            oOutputParameterHandler.addParameter('sThumb', sThumb)
            oGui.addMisc(SITE_IDENTIFIER, 'showHosters', sTitle, '', sThumb, sDesc, oOutputParameterHandler)

    oGui.setEndOfDirectory()


def showHosters():
    oGui = cGui()
    oParser = cParser()
    oInputParameterHandler = cInputParameterHandler()
    sUrl = oInputParameterHandler.getValue('siteUrl')
    sMovieTitle = oInputParameterHandler.getValue('sMovieTitle')
    sThumb = oInputParameterHandler.getValue('sThumb')

    sHosterUrl = ''

    oRequestHandler = cRequestHandler(sUrl)
    sHtmlContent = oRequestHandler.request()

    sPattern = "redirectUrl='(.+?)';"
    aResult = oParser.parse(sHtmlContent, sPattern)
    if (aResult[0]):
        sUrl = aResult[1][0]

    oRequestHandler = cRequestHandler(sUrl)
    oRequestHandler.addHeaderEntry('User-Agent', UA)
    oRequestHandler.addHeaderEntry('Referer', 'https://yallalive.id/')
    oRequestHandler.addHeaderEntry('Origin', 'yallalive.id')
    sHtmlContent = oRequestHandler.request()

    sPattern = 'href="(.+?)" target="search_iframe">(.+?)</a>'
    aResult = oParser.parse(sHtmlContent, sPattern)
    if aResult[0]:
        for aEntry in aResult[1]:
            sTitle = sMovieTitle+' '+aEntry[1]
            url = aEntry[0]
            if '.m3u8' in url:
                sHosterUrl = url.split('=')[1]
            if 'embed' in url:
                oRequestHandler = oRequestHandler.request()
                sHtmlContent2 = oRequestHandler.request()

                sPattern = 'src="(.+?)" scrolling="no">'
                aResult = oParser.parse(sHtmlContent2, sPattern)
                if aResult[0]:
                    sHosterUrl = aResult[1][0]
            if '/dash/' in url:
                oRequestHandler = oRequestHandler.request()
                sHtmlContent4 = oRequestHandler.request()
                regx = '''var s = '(.+?)';.+?url="(.+?)".+?s;'''
                var = re.findall(regx, sHtmlContent4, re.S)
                if var:
                    a = var[0][0]
                    a = a.replace('\\', '')
                    b = var[0][1]
                    url = 'https://video-a-sjc.xx.fbcdn.net/hvideo-ash66'+a
                sHosterUrl = url + '|Referer=' + URL_MAIN
                Referer = aEntry[0].split('live')[0]

            if 'amazonaws.com' in url:
                sHosterUrl = url + '|User-Agent=' + UA + '&Referer='+Referer
            if 'vimeo' in url:
                sHosterUrl = url + "|Referer=" + sUrl

            if 'sharecast' in url:
                Referer = "https://sharecast.ws/"
                sHosterUrl = Hoster_ShareCast(url, Referer)

            if 'tastyturbine' in url:
                Referer = url
                sHosterUrl = Hoster_ShareCast(url, Referer)

            if 'elegantpelican' in url:
                Referer = url
                sHosterUrl = Hoster_ShareCast(url, Referer)

            oHoster = cHosterGui().checkHoster(sHosterUrl)
            if oHoster:
                oHoster.setDisplayName(sTitle)
                oHoster.setFileName(sMovieTitle)
                cHosterGui().showHoster(oGui, oHoster, sHosterUrl, sThumb)

            else:
                sHosterUrl = getHosterIframe(url, sUrl)

                oHoster = cHosterGui().checkHoster(sHosterUrl)
                if oHoster:
                    oHoster.setDisplayName(sTitle)
                    oHoster.setFileName(sMovieTitle)
                    cHosterGui().showHoster(oGui, oHoster, sHosterUrl, sThumb)

    sPattern = "'link': u'(.+?)',"
    aResult = oParser.parse(sHtmlContent, sPattern)
    if aResult[0]:
        for aEntry in aResult[1]:

            url = aEntry
            if '.php?' in url:
                oRequestHandler = oRequestHandler.request()
                sHtmlContent2 = oRequestHandler.request()

                sPattern = 'source: "(.+?)",'
                aResult = oParser.parse(sHtmlContent2, sPattern)
                if aResult[0]:
                    url = aResult[1][0]
            if 'embed' in url:
                oRequestHandler = oRequestHandler.request()
                sHtmlContent2 = oRequestHandler.request()

                sPattern = 'src="(.+?)" scrolling="no">'
                aResult = oParser.parse(sHtmlContent2, sPattern)
                if aResult[0]:
                    url = aResult[1][0]
            if 'multi.html' in url:
                url2 = url.split('=')
                live = url2[1].replace("&ch", "")
                ch = url2[2]
                oRequestHandler = oRequestHandler.request()
                sHtmlContent2 = oRequestHandler.request()

                sPattern = "var src = (.+?),"
                aResult = oParser.parse(sHtmlContent2, sPattern)
                if aResult[0]:
                    url2 = aResult[1][0].split('hls:')
                    url2 = url2[1].split('+')
                    url2 = url2[0].replace("'", "")
                    url = url2+live+'/'+ch+'.m3u8'
            if '/dash/' in url:
                oRequestHandler = oRequestHandler.request()
                sHtmlContent4 = oRequestHandler.request()
                regx = '''var s = '(.+?)';.+?url="(.+?)".+?s;'''
                var = re.findall(regx, sHtmlContent4, re.S)
                if var:
                    a = var[0][0]
                    a = a.replace('\\', '')
                    b = var[0][1]
                    url = 'https://video-a-sjc.xx.fbcdn.net/hvideo-ash66'+a
            sHosterUrl = url + '|User-Agent=' + UA + '&Referer=' + URL_MAIN
            sMovieTitle = 'link'
            if 'vimeo' in sHosterUrl:
                sHosterUrl = sHosterUrl + "|Referer=" + sUrl

            oHoster = cHosterGui().checkHoster(sHosterUrl)
            if oHoster:
                oHoster.setDisplayName(sMovieTitle+' '+sTitle)
                oHoster.setFileName(sMovieTitle)
                cHosterGui().showHoster(oGui, oHoster, sHosterUrl, sThumb)

    oGui.setEndOfDirectory()


def Hoster_ShareCast(url, referer):
    oRequestHandler = cRequestHandler(url)
    oRequestHandler.addHeaderEntry('User-Agent', UA)
    oRequestHandler.addHeaderEntry('Referer', referer)
    sHtmlContent = oRequestHandler.request()

    sPattern = 'new Player\(.+?player","([^"]+)",{"([^"]+)'
    aResult = re.findall(sPattern, sHtmlContent)

    if aResult:
        site = 'https://' + aResult[0][1]
        url = (site + '/hls/' + aResult[0][0] + '/live.m3u8') + '|Referer=' + Quote(site)
        return url

    return False, False


def getHosterIframe(url, referer):
    oRequestHandler = cRequestHandler(url)
    oRequestHandler.addHeaderEntry('Referer', referer)
    sHtmlContent = str(oRequestHandler.request())
    if not sHtmlContent:
        return False

    referer = url
    if 'channel' in referer:
        referer = referer.split('channel')[0]

    sPattern = '(\s*eval\s*\(\s*function(?:.|\s)+?{}\)\))'
    aResult = re.findall(sPattern, sHtmlContent)
    if aResult:
        sstr = aResult[0]
        if not sstr.endswith(';'):
            sstr = sstr + ';'
        sHtmlContent = cPacker().unpack(sstr)

    sPattern = '.atob\("(.+?)"'
    aResult = re.findall(sPattern, sHtmlContent)
    if aResult:
        code = aResult[0]
        try:
            if isMatrix():
                code = base64.b64decode(code).decode('ascii')
            else:
                code = base64.b64decode(code)
            return code + '|Referer=' + referer
        except Exception as e:
            pass

    sPattern = '<iframe.+?src=["\']([^"\']+)["\']'
    aResult = re.findall(sPattern, sHtmlContent)
    if aResult:
        for url in aResult:
            if url.startswith("./"):
                url = url[1:]
            if not url.startswith("http"):
                if not url.startswith("//"):
                    url = '//'+referer.split('/')[2] + url
                url = "https:" + url
            url = getHosterIframe(url, referer)
            if url:
                return url

    sPattern = "onload=\"ThePlayerJS\('.+?',\s*'([^\']+)"
    aResult = re.findall(sPattern, sHtmlContent)
    if aResult:
        url2 = 'https://catastrophicfailure.dev/player/' + aResult[0]
        url2 = Hoster_ShareCast(url2, url)
        if url2:
            return url2

    sPattern = 'src=["\']([^"\']+)["\']'
    aResult = re.findall(sPattern, sHtmlContent)

    if aResult:
        url = aResult[0]
        if '.m3u8' in url:
            return url

    sPattern = "new Player\(.+?player\",\"([^\"]+)\",{'([^\']+)"
    aResult = re.findall(sPattern, sHtmlContent)
    if aResult:
        site = 'https://' + aResult[0][1]
        url = (site + '/hls/' + aResult[0][0] + '/live.m3u8') + '|Referer=' + Quote(site)
        return url

    sPattern = '<iframe.+?src=["\']([^"\']+)["\']'
    aResult = re.findall(sPattern, sHtmlContent)
    if aResult:
        return aResult[0] + '|Referer='+referer

    sPattern = 'source:\s*["\']([^"\']+)["\']'
    aResult = re.findall(sPattern, sHtmlContent)
    if aResult:
        return aResult[0] + '|referer=' + referer

    sPattern = '[^/]source.+?["\'](https.+?)["\']'
    aResult = re.findall(sPattern, sHtmlContent)
    if aResult:
        return aResult[0] + '|referer=' + referer

    sPattern = 'file: *["\'](https.+?\.m3u8)["\']'
    aResult = re.findall(sPattern, sHtmlContent)
    if aResult:
        return aResult[0] + '|referer=' + referer

    sPattern = '[^/]source.+?["\'](https.+?)["\']'
    aResult = re.findall(sPattern, sHtmlContent)
    if aResult:
        return True, aResult[0] + '|referer=' + url

    return False
