# -*- coding: utf-8 -*-
# https://github.com/Kodi-vStream/venom-xbmc-addons
import base64
import re

from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import (VSlog,
                                                                    isMatrix)
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.requestHandler import \
    cRequestHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.hunter import hunter
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser

UA = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0'


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, 'upvideo', 'UpVideo')

    def _getMediaLinkForGuest(self):
        api_call = False
        VSlog(self._url)
        oParser = cParser()
        sPattern = 'return decodeURIComponent\(escape\(r\)\)}\("([^,]+)",([^,]+),"([^,]+)",([^,]+),([^,]+),([^,\))]+)\)'

        oRequest = cRequestHandler(self._url)
        oRequest.addHeaderEntry('Cookie', 'popads2=opened')
        sHtmlContent = oRequest.request()

        aResult = oParser.parse(sHtmlContent, sPattern)
        if aResult[0]:
            l = aResult[1]
            for j in l:
                data = hunter(j[0], int(j[1]), j[2], int(j[3]), int(j[4]), int(j[5]))
                if "fcbbbdddebad" in data:
                    r = re.search('var fcbbbdddebad *= *"([^"]+)" *;', data)
                    if not r:
                        VSlog('er2')
                    v2 = r.group(1).split('aHR0')[1].split('YTk0NT')[0]

                    if isMatrix():
                        api_call = "htt" + (base64.b64decode(v2).decode())
                    else:
                        api_call = "htt" + base64.b64decode(v2)

        if api_call:
            return True, api_call

        return False, False
