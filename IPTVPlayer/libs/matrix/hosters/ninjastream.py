# -*- coding: utf-8 -*-
# vStream https://github.com/Kodi-vStream/venom-xbmc-addons

from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import VSlog
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.requestHandler import \
    cRequestHandler

UA = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) ' + \
    'Chrome/83.0.4103.116 Safari/537.36'


class cHoster(iHoster):
    def __init__(self):
        iHoster.__init__(self, 'ninjastream', 'NinjaStream')

    # facultatif mais a laisser pour compatibilitee
    # Extraction du lien et decodage si besoin
    def _getMediaLinkForGuest(self):
        VSlog(self._url)
        oRequestHandler = cRequestHandler("https://ninjastream.to/api/video/get")
        oRequestHandler.setRequestType(1)
        oRequestHandler.addHeaderEntry('Referer', self._url)
        oRequestHandler.addHeaderEntry('User-Agent', UA)
        oRequestHandler.addHeaderEntry('X-Requested-With', 'XMLHttpRequest')
        oRequestHandler.addHeaderEntry('Origin', 'https://{0}'.format(self._url.split('/')[2]))
        oRequestHandler.addJSONEntry('id', self._url.split('/')[4])
        sHtmlContent = oRequestHandler.request(jsonDecode=True)

        api_call = sHtmlContent['result']['playlist']

        if api_call:
            return True, api_call

        return False, False
