# -*- coding: utf-8 -*-

import re

import requests
from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib import helpers, random_ua
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import VSlog
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.packer import cPacker

UA = random_ua.get_pc_ua()


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, 'lulustream', 'Lulustream')

    def _getMediaLinkForGuest(self):
        VSlog(self._url)

        headers = {
            'User-Agent': UA,
            'Origin': self._url.rsplit('/', 1)[0],
            'Referer': self._url}
        s = requests.session()
        sHtmlContent = s.get(self._url, headers=headers).text

        api_call = ''

        aResult = re.search(r'(\s*eval\s*\(\s*function\(p,a,c,k,e(?:.|\s)+?)<\/script>', sHtmlContent)
        if aResult:
            sHtmlContent = cPacker().unpack(aResult.group(1))

        aResult = re.search(r'sources:\s*\[{file:\s*["\']([^"\']+)', sHtmlContent)
        if aResult:
            api_call = aResult.group(1)

        if api_call:
            return True, api_call + helpers.append_headers(headers)

        return False, False
