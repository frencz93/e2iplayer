# -*- coding: utf-8 -*-
# https://github.com/Kodi-vStream/venom-xbmc-addons
#
import re

from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import VSlog
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.GKDecrypter import \
    GKDecrypter
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.requestHandler import \
    cRequestHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.packer import cPacker
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser

# https://forums.tvaddons.ag/tknorris-release-repository/10792-debugging-daclips-2.html


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, 'vidbull', 'VidBull')

    def _getMediaLinkForGuest(self):
        oParser = cParser()
        VSlog(self._url)

        url_stream = ''

        oRequest = cRequestHandler(self._url)
        sHtmlContent = oRequest.request()

        sPattern = "<script type='text\/javascript'>(eval\(function\(p,a,c,k,e,d.+?)<\/script>"
        aResult = oParser.parse(sHtmlContent, sPattern)

        if aResult[0]:
            for i in aResult[1]:
                sHtmlContent = cPacker().unpack(i)

                # Premiere methode avec <embed>
                if '<embed' in sHtmlContent:
                    pass

                # deuxieme methode, lien code aes
                else:
                    EncodedLink = re.search('file:"([^"]+)"', sHtmlContent, re.DOTALL)

                    if (EncodedLink):

                        Key = "a949376e37b369" + "f17bc7d3c7a04c5721"
                        x = GKDecrypter(128, 128)
                        sUrl = x.decrypt(EncodedLink.group(1), Key.decode("hex"), "ECB").split('\0')[0]

                        url_stream = sUrl

        if (url_stream):
            return True, url_stream
        else:
            return False, False

        return False, False
