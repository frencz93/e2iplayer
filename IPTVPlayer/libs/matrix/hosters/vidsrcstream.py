# -*- coding: utf-8 -*-

import base64
import re

import requests
from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import VSlog

UA = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0'


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, 'vidsrcstream', 'VidsrcStrem')

    def _getMediaLinkForGuest(self):
        VSlog(self._url)
        referer = self._url
        if 'm3u8' in self._url:
            return True, self._url

        if '?Referer=' in self._url:
            referer = self._url.split('?Referer=')[1]
            self._url = self._url.split('?Referer=')[0]

        headers = {'User-Agent': UA, 'Referer': referer}
        s = requests.session()

        req = s.get(self._url, headers=headers)

        hls_url = re.search(r'file:"([^"]*)"', req.text).group(1)
        hls_url = re.sub(r'\/\/\S+?=', '', hls_url).replace('#2', '')

        try:
            hls_url = base64.b64decode(hls_url).decode('utf-8')
        except Exception:
            return self._getMediaLinkForGuest(self._url + f'?Referer={referer}')

        set_pass = re.search(r'var pass_path = "(.*?)";', req.text).group(1)
        if set_pass.startswith("//"):
            set_pass = f"https:{set_pass}"

        if hls_url:
            return True, hls_url + '|User-Agent=' + UA + '&Referer=' + referer

        return False, False
