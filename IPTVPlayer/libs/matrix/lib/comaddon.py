# -*- coding: utf-8 -*-
# https://github.com/Kodi-vStream/venom-xbmc-addons
import json
import re

from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (GetCacheSubDir,
                                                           GetDefaultLang,
                                                           printDBG)


# class addon(xbmcaddon.Addon):
class none_(object):
    def __new__(cls, *args):
        return object.__new__(cls)

    def __init__(self, *args):
        pass

    def __getattr__(self, name):
        return self

    def __call__(self, *args, **kwargs):
        return self

    def __int__(self):
        return 0

    def __float__(self):
        return 0

    def __str__(self):
        return '0'

    def __nonzero__(self):
        return False

    def __getitem__(self, key):
        return self

    def __setitem__(self, key, value):
        pass

    def __delitem__(self, key):
        pass

    def __len__(self):
        return 3

    def __iter__(self):
        return iter([self, self, self])


class listitem():
    def __init__(self, label='', label2='', iconImage='', thumbnailImage='', path=''):
        self.items = {}
        self.type = 'none'

    def setInfo(self, type_, infoLabels):
        self.type = type_
        self.items.update(infoLabels)

    def setArt(self, artLabels):
        self.items.update(artLabels)

    def setProperty(self, key, value):
        self.items.update({key: value})

    def getProperty(self, key):
        return self.items.get(key, '')

    def addContextMenuItems(self, items):
        pass

    def getItems(self):
        return self.items


class addon():
    def __init__(self, addonId=None):
        self.addonId = addonId
        self.settings = {}
        self.openSettings()

    def openSettings(self):
        settings_path = '/usr/lib/enigma2/python/Plugins/Extensions/IPTVPlayer/libs/matrix/settings.json'
        with open(settings_path) as json_file:
            self.settings = json.load(json_file)

    def getSetting(self, key):
        out = self.settings.get(key, None)
        if out == None:
            VSlog('settings:'+key+' notfound')
            return "false"
        return str(out)

    def setSetting(self, key, value):
        if key == 'ZT':
            f = open(GetCacheSubDir('Tsiplayer')+'zt.url', "w")
            f.write(value)
            f.close()
        return None

    def getAddonInfo(self, info):
        return None

    def VSlang(self, lang):
        if GetDefaultLang() == 'en':
            lng = 'English'
        elif GetDefaultLang() == 'fr':
            lng = 'French'
        elif GetDefaultLang() == 'ar':
            lng = 'Arabic'
        else:
            lng = 'English'
        f = open('/usr/lib/enigma2/python/Plugins/Extensions/IPTVPlayer/libs/matrix/language/'+lng+'/strings.po', "r")
        data = f.read()
        str_out = str(lang)
        lst_data = re.findall('msgctxt.*?"(.*?)".*?msgid.*?"(.*?)".*?msgstr.*?"(.*?)".*?', data, re.S)
        for (msgctxt, msgid, msgstr) in lst_data:
            if (msgctxt.replace('#', '').strip() == str(lang)):
                if msgstr.strip() != '':
                    str_out = msgstr
                else:
                    str_out = msgid
                break
        return str_out.replace("70012", "Kids")


class dialog():
    def VSselectqual(self, list_qual, list_url):
        VSlog('start:'+str(list_url))
        if len(list_url) == 0:
            return ''
        if len(list_url) == 1:
            return list_url[0]
        ret = 0
        i = 0
        urlout = ''
        for url in list_url:
            urlout = urlout+url+'|tag:'+list_qual[i]+'||'
            i = i+1
        VSlog('start:'+str(urlout))
        return urlout

    def VSinfo(self, desc, title='matrix', iseconds=0, sound=False):
        return ''

    def VSerror(self, e):
        printDBG('VSerror: '+str(e))
        return

    def VSok(self, e):
        printDBG('VSok: '+str(e))
        return


class progress():
    def VScreate(self, title='matrix', desc='', large=False):
        return self

    def VSupdate(self, dialog, total, text='', search=False):
        count = 0

    def VSupdatesearch(self, dialog, total, text=''):
        count = 0

    def VSclose(self, dialog=''):
        return

    def iscanceled(self):
        return False


class window():
    def __init__(self, winID):
        pass

    def getProperty(self, prop):
        return 'false'

    def clearProperty(self, prop):
        return ''

    def setProperty(self, prop, val):
        return ''


class siteManager():

    SITES = 'sites'
    ACTIVE = 'active'
    CLOUDFLARE = 'cloudflare'
    LABEL = 'label'
    URL_MAIN = 'url'
    URL_MAIN2 = 'url2'

    def __init__(self):
        self.defaultPath = VSPath('/usr/lib/enigma2/python/Plugins/Extensions/IPTVPlayer/libs/matrix/sites.json')
        self.defaultData = None

        try:
            self.propertiesPath = VSPath(self.defaultPath).decode('utf-8')
        except AttributeError:
            self.propertiesPath = VSPath(self.defaultPath)

        with open(self.propertiesPath, 'r') as f:
            self.data = json.load(f)

    def isActive(self, sourceName):
        return self.getProperty(sourceName, self.ACTIVE) == 'True'

    def setActive(self, sourceName, state):
        self.setProperty(sourceName, self.ACTIVE, state)

    def isEnable(self, sourceName):
        return self.getDefaultProperty(sourceName, self.ACTIVE) == 'True'

    # site identifié par la team comme étant protégé par Cloudflare, false par défaut si non renseigné
    def isCloudFlare(self, sourceName):
        return self.getDefaultProperty(sourceName, self.CLOUDFLARE) == 'True'

    def getUrlMain(self, sourceName):
        return str(self.getDefaultProperty(sourceName, self.URL_MAIN))

    def getUrlMain2(self, sourceName):
        return str(self.getDefaultProperty(sourceName, self.URL_MAIN2))

    def disableAll(self):
        for sourceName in self.data[self.SITES]:
            self.setActive(sourceName, False)
        return

    def enableAll(self):
        for sourceName in self.data[self.SITES]:
            self.setActive(sourceName, True)
        return

    def _getDataSource(self, sourceName):
        sourceData = self.data[self.SITES].get(sourceName)
        if not sourceData:
            sourceData = self._getDefaultProp(sourceName)
            if sourceData:
                self.data[self.SITES][sourceName] = sourceData

        return sourceData

    def _getDefaultProp(self, sourceName):
        if not self.defaultData:
            with open(self.defaultPath, 'r') as f:
                self.defaultData = json.load(f)

        sourceData = self.defaultData[self.SITES].get(sourceName) if self.defaultData and self.SITES in self.defaultData else None
        if not sourceData:
            return {}

        return sourceData

    def getDefaultProperty(self, sourceName, propName):
        defaultProps = self._getDefaultProp(sourceName)
        if propName not in defaultProps:
            return False
        return defaultProps.get(propName)

    def getProperty(self, sourceName, propName):
        sourceData = self._getDataSource(sourceName)
        if sourceData:
            if propName in sourceData:
                return sourceData.get(propName)

    def setProperty(self, sourceName, propName, value):
        sourceData = self._getDataSource(sourceName)
        if sourceData:
            sourceData[propName] = str(value)


def VSProfil():
    return 'Master user'


def CountdownDialog():
    return False


def VSlog(e, level=''):
    printDBG('VSlog: '+str(e))
    return


def isKrypton():
    return False


def isMatrix():
    return True


def isNexus():
    return False


def VSPath(path):
    path = path.replace('special://temp/', GetCacheSubDir('Tsiplayer'))
    path = path.replace('special://home/userdata/addon_data/plugin.video.matrix/', GetCacheSubDir('Tsiplayer'))
    return path


def VSupdate():
    return ''
