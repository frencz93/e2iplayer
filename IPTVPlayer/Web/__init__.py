# -*- coding: utf-8 -*-
# WebComponent will use own translation files to simplify management.

from gettext import bindtextdomain, dgettext, gettext

from Components.Language import language
from Tools.Directories import SCOPE_PLUGINS, resolveFilename

PluginLanguageDomain = "IPTVPlayerWebComponent"
PluginLanguagePath = "Extensions/IPTVPlayer/Web/locale"


def localeInit():
	bindtextdomain(PluginLanguageDomain, resolveFilename(SCOPE_PLUGINS, PluginLanguagePath))


def _(txt):
	t = dgettext(PluginLanguageDomain, txt)
	if t == txt:
		t = dgettext("IPTVPlayer", txt)
		if t == txt:
			t = gettext(txt)
	return t


localeInit()
language.addCallback(localeInit)
