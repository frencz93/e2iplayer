#!/bin/bash
# ###########################################
# SCRIPT : DOWNLOAD AND INSTALL E2IPLAYER_TSiplayer
# ###########################################
#
# Command:
#     wget -qO- --no-check-certificate https://gitlab.com/MOHAMED_OS/e2iplayer/-/raw/main/install-e2iplayer.sh?inline=false -qO - | bash
#          OR:
#     curl -LJso- https://gitlab.com/MOHAMED_OS/e2iplayer/-/raw/main/install-e2iplayer.sh?inline=false | bash
#
# ###########################################

# Colors
Color_Off='\e[0m'
Red='\e[0;31m'
Green='\e[0;32m'
Yellow='\e[0;33m'

curl --help >/dev/null 2>&1 && DOWNLOADER="curl -L -J -s -o" || DOWNLOADER="wget --no-check-certificate -q -O"

###########################################
# Configure where we can find things here #
TMPDIR='/tmp/'
PLUGINPATH='/usr/lib/enigma2/python/Plugins/Extensions/'
SETTINGS='/etc/enigma2/settings'
URL='https://gitlab.com/MOHAMED_OS/e2iplayer/-'
MY_PATH='/media/mmc/'
FileTar='e2iplayer-main.tar.gz'
pyVersion=$(python -c"from sys import version_info; print(version_info[0])")

#########################
VERSION=$(${DOWNLOADER} - $URL/raw/main/update2/lastversion.php | awk 'NR==1')
arrVar=("enigma2-plugin-extensions-e2iplayer-deps" "exteplayer3" "gstplayer")

if [ "$pyVersion" = 3 ]; then
  arrVar+=("python3-sqlite3" "python3-pycurl" "python3-json" "python3-requests")
else
  arrVar+=("python-sqlite3" "python-pycurl" "python-e2icjson" "python-json" "python-requests" "cmdwrap")
fi
########################
if [ -f /etc/opkg/opkg.conf ]; then
  Status='/var/lib/opkg/status'
  OStype='Opensource'
  Update='opkg update'
  OSinstall='opkg install'
elif [ -f /etc/apt/apt.conf ]; then
  Status='/var/lib/dpkg/status'
  OStype='DreamOS'
  Update='apt-get update'
  OSinstall='apt-get install --fix-broken --yes --assume-yes'
fi

#########################
case $(uname -m) in
armv7l*) plarform="armv7" ;;
mips*) plarform="mipsel" ;;
sh4*) plarform="sh4" ;;
aarch64*) plarform="aarch64" ;;
esac

#########################
$Update >/dev/null 2>&1

install() {
  if ! grep -qs "Package: $1" "$Status"; then
    echo -e "   >>>>   Please Wait to install ${Yellow} $1 ${Color_Off}  <<<<"
    sleep 0.8
    echo
    $OSinstall "$1"
    sleep 0.8
    clear
  fi
}

#########################
clear

if [ -e ${TMPDIR}${FileTar} ]; then
  echo -e "${Red}" "remove archive file" "${Color_Off}"
  rm -f ${TMPDIR}${FileTar}
fi

if [ -e ${TMPDIR}e2iplayer-main ]; then
  rm -fr ${TMPDIR}e2iplayer-main
fi

echo -e "${Yellow}" "Downloading E2iPlayer plugin Please Wait ......" "${Color_Off}"
${DOWNLOADER} "${TMPDIR}${FileTar}" "${URL}/archive/main/${FileTar}"
wait

if ! [ -e ${TMPDIR}${FileTar} ]; then
  echo -e "${Red}" "error downloading archive, end" "${Color_Off}"
  exit 1
else
  echo -e "${Green}" "Archive downloaded" "${Color_Off}"
fi

tar -xzf ${TMPDIR}${FileTar} -C ${TMPDIR}
if ! [ -e "${TMPDIR}e2iplayer-main" ]; then
  echo -e "${Red}" "error extracting archive, end" "${Color_Off}"
else
  echo -e "${Green}" "Archive extracted" "${Color_Off}"
  rm -f ${TMPDIR}${FileTar}
fi

if [ -e "${PLUGINPATH}IPTVPlayer" ]; then
  if [ -e "${PLUGINPATH}IPTVPlayer/libs/iptvsubparser/_subparser.so" ]; then
    cp -rf "${PLUGINPATH}IPTVPlayer/libs/iptvsubparser/_subparser.so" ${TMPDIR}
  fi
  if [ -e "${PLUGINPATH}IPTVPlayer/libs/e2icjson/e2icjson.so" ]; then
    cp -rf "${PLUGINPATH}IPTVPlayer/libs/e2icjson/e2icjson.so" ${TMPDIR}
  fi
  echo -e "${Red}" "Removed old version of E2Iplayer" "${Color_Off}"
  rm -rf ${PLUGINPATH}IPTVPlayer
fi

if [ "$pyVersion" = 3 ]; then
  PYcurl="python3-pycurl"
else
  PYcurl="python-pycurl"
fi

if ! grep -qs "Package: $PYcurl" "$Status"; then
  ${DOWNLOADER} - "https://gitlab.com/MOHAMED_OS/resourcese2iplayer/-/raw/main/resources/pycurlinstall.py?inline=false" | python
fi

cp -rf ${TMPDIR}e2iplayer-main/tsiplayer_xtream.conf /etc
cp -rf ${TMPDIR}e2iplayer-main/IPTVPlayer ${PLUGINPATH}
if [ -e "${TMPDIR}_subparser.so" ]; then
  cp -rf ${TMPDIR}_subparser.so "${PLUGINPATH}IPTVPlayer/libs/iptvsubparser/"
fi
if [ -e "${TMPDIR}e2icjson.so" ]; then
  cp -rf ${TMPDIR}e2icjson.so "${PLUGINPATH}IPTVPlayer/libs/e2icjso/"
fi

if ! [ -e "${PLUGINPATH}IPTVPlayer" ]; then
  echo -e "${Red}" "error installing E2Iplayer, end" "${Color_Off}"
  exit 1
else
  echo -e "${Green}" "E2Iplayer installed" "${Color_Off}"
  rm -fr ${TMPDIR}e2iplayer-main
fi

sleep 3
#########################
if [ -e "${MY_PATH}iptvplayer.sh" ]; then
  SciezkaCache="${MY_PATH}IPTVCache/"
  NaszaSciezka="${MY_PATH}Player/"
  Sciezkaurllist="${MY_PATH}File/"
else
  SciezkaCache='/etc/IPTVCache/'
  NaszaSciezka='/hdd/movie/'
  Sciezkaurllist="/hdd/"
fi
#########################
if [ -e "${PLUGINPATH}IPTVPlayer" ]; then
  echo -e ":Your Device IS ${Yellow} $(uname -m) ${Color_Off} processor ..."
  echo "Add Setting To ${SETTINGS} ..."
  init 4
  sleep 5
  sed -e s/config.plugins.iptvplayer.*//g -i ${SETTINGS}
  sleep 2
  {
    echo "config.plugins.iptvplayer.AktualizacjaWmenu=true"
    echo "config.plugins.iptvplayer.alternative${plarform^^}MoviePlayer=extgstplayer"
    echo "config.plugins.iptvplayer.alternative${plarform^^}MoviePlayer0=extgstplayer"
    echo "config.plugins.iptvplayer.buforowanie_m3u8=false"
    echo "config.plugins.iptvplayer.cmdwrappath=/usr/bin/cmdwrap"
    echo "config.plugins.iptvplayer.debugprint=/tmp/iptv.dbg"
    echo "config.plugins.iptvplayer.default${plarform^^}MoviePlayer=exteplayer"
    echo "config.plugins.iptvplayer.default${plarform^^}MoviePlayer0=exteplayer"
    echo "config.plugins.iptvplayer.dukpath=/usr/lib/enigma2/python/Plugins/Extensions/IPTVPlayer/${plarform}/bin/duk"
    echo "config.plugins.iptvplayer.extplayer_infobanner_clockformat=24"
    echo "config.plugins.iptvplayer.extplayer_skin=line"
    echo "config.plugins.iptvplayer.f4mdumppath=/usr/bin/f4mdump"
    echo "config.plugins.iptvplayer.gstplayerpath=/usr/bin/gstplayer"
    echo "config.plugins.iptvplayer.hlsdlpath=/usr/bin/hlsdl"
    echo "config.plugins.iptvplayer.NaszaSciezka=${NaszaSciezka}"
    echo "config.plugins.iptvplayer.opensuborg_login=MOHAMED_OS"
    echo "config.plugins.iptvplayer.opensuborg_password=&ghost@mcee2017&"
    echo "config.plugins.iptvplayer.osk_type=system"
    echo "config.plugins.iptvplayer.plarform=${plarform}"
    echo "config.plugins.iptvplayer.remember_last_position=true"
    echo "config.plugins.iptvplayer.rtmpdumppath=/usr/bin/rtmpdump"
    echo "config.plugins.iptvplayer.SciezkaCache=${SciezkaCache}"
    echo "config.plugins.iptvplayer.Sciezkaurllist=${Sciezkaurllist}"
    echo "config.plugins.iptvplayer.uchardetpath=/usr/bin/uchardet"
    echo "config.plugins.iptvplayer.updateLastCheckedVersion=${VERSION}"
    echo "config.plugins.iptvplayer.usepycurl=True"
    echo "config.plugins.iptvplayer.watched_item_color=#FF0000"
    echo "config.plugins.iptvplayer.wgetpath=wget"
  } >>${SETTINGS}
fi

#########################
if [ -e "${MY_PATH}File/tsiplayer_xtream.conf" ]; then
  rm -rf /etc/tsiplayer_xtream.conf
  sleep 0.8
  cp -r "${MY_PATH}File/tsiplayer_xtream.conf" /etc/
fi

#########################
for i in "${arrVar[@]}"; do
  install "$i"
done
######

sync
echo ""
echo "***********************************************************************"
echo "**                                                                    *"
echo "**                       E2iPlayer  : $VERSION                   *"
echo "**                       Script by  : MOHAMED_OS                      *"
echo "**  Support    : https://www.tunisia-sat.com/forums/threads/3951696/  *"
echo "**                                                                    *"
echo "***********************************************************************"
echo ""

sleep 0.8
echo -e "${Yellow}" "Device will restart now" "${Color_Off}"
if [ "$OStype" = "DreamOS" ]; then
  sleep 2
  reboot -f
else
  init 4
  sleep 2
  init 3
fi

exit 0
